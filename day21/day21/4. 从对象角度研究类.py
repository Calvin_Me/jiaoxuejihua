# class Human:
#     """
#     类的具体结构
#     """
#     # 第一部分:静态属性
#     mind = '有思想'   # 类的属性  (静态属性, 静态字段)
#     language = '使用语言'
#
#     def __init__(self):
#         # print(f'self---->: {self}')
#         # print(666)
#         self.name = '李业'
#         self.age = 18
#
#     # 第二部分: 动态方法
#     def work(self):
#         print('人类都会工作')
#
#     def eat(self):
#         print('人类都需要吃饭')
#
# obj = Human()  # 实例化过程
# 得到一个返回值,这个返回值就是 对象,实例.
# print(f'obj---> {obj}')
# 实例化一个对象发生了三件事:
'''
    1. 开辟一个对象空间.
    2. 自动执行__init__方法,并且将对象地址传给self.
    3. 运行__init__方法内的代码,给对象空间封装属性.

'''



class Human:
    """
    类的具体结构
    """
    # 第一部分:静态属性
    mind = '有思想'   # 类的属性  (静态属性, 静态字段)
    language = '使用语言'

    def __init__(self, name, age):
        # print(f'self---->: {self}')
        # print(666)
        self.n = name
        self.a = age


    # 第二部分: 动态方法
    def work(self):
        # print(f'self---> {self}')
        print(f'{self.n}都会工作')


    def eat(self):
        print(f'{self.n}都需要吃饭')

# obj = Human('李业',18)  # 实例化过程
# print(obj.n)
# print(obj.a)
# print(obj.__dict__)

# 一:对象操作对象空间的属性

# 1. 对象查看对象的空间的所有属性
# obj = Human('李业',18)
# print(obj.__dict__)

# 2. 对象操作对象空间的属性
# obj = Human('李业',18)
# 增:
# obj.sex = 'laddy_boy'
# 删:
# del obj.a
# 改:
# obj.a = 1000
# 查:
# print(obj.n)
# print(obj.__dict__)

# 二 对象查看类中的属性
# obj = Human('李业',18)
# # print(obj.mind)
# obj.mind = '无脑的'
# print(obj.mind)
# print(Human.mind)

# 三 对象调用类中的方法
# obj = Human('孙戴维', 23)
# # print(f'obj---> {obj}')
# obj.work()
# obj.eat()

# 一个类可以实例化多个对象
# obj1 = Human('李业',18)
# obj2 = Human('小可爱', 16)
# obj3 = Human('怼姐', 18)

# 变量,函数名:
# age_of_oldboy = 73
# Ageofoldboy