import re
# s = "meet_宝元_meet"
# print(re.findall("meet",s))
# 从字符串中全部查找内容,返回一个列表


# s = "meet_宝元_meet123"
# print(re.findall("\w",s))
# 查找数字,字母(中文),下划线


# s = "meet_宝元_meet123!@#"
# print(re.findall("\W",s))
# 查找非数字,字母(中文),下划线


# s = "meet_  宝元_   me   et\t \n"
# print(re.findall("\s",s))
# 查找任意空格,换行符,制表符


# s = "meet_  宝元_   me   et\t \n"
# print(re.findall("\S",s))
# 查找非任意空格,换行符,制表符


# s = "meet_  宝元_123me   et\t \n"
# print(re.findall("\d",s))
# 查找数字

# print(re.findall("\D",s))
# 查找非数字


# s = "meet宝元_123meet\t \n"
# print(re.findall("\Ameet",s))
# print(re.findall("^meet",s))
# 查找是否以什么开头的内容


# s = "meet宝元_123meet"
# print(re.findall("t\Z",s))
# print(re.findall("t$",s))
# 查找是否以什么结尾的内容

# s = "meet宝元_123meet \n \t \n"
# print(re.findall("\n",s))
# 查找换行符

# print(re.findall("\t",s))
# 查找制表符

# s = "m\net宝元_123maet \n \t "
# print(re.findall("m.e",s))
# .只能匹配任意一个内容(非换行符)

# s = "m\net宝元_123maet \n \t "
# print(re.findall("m.e",s,re.DOTALL))
# .只能匹配任意一个内容

# s = "meet宝元_1A-23maet"
# print(re.findall("[a-z]",s))  # 小写的a,z
# print(re.findall("[A-Z]",s))  # 大写的A,Z
# print(re.findall("[A-Za-z]",s)) # 大写和小写的a,z A,Z
# print(re.findall("[a-z0-9]",s)) # 小写的a,z 数字 0,9

# s = "meet宝元_1A-23maet"
# print(re.findall("[^0-9]",s))   # [^0-9] 查找非0-9的内容

# s = "mmmmmm"
# print(re.findall("m*",s))  # * 匹配 0个多个  [贪婪匹配]


# s = "meet_asdf_msss_mmns_aaam" # + 匹配1个或多个 [贪婪匹配]
# print(re.findall("me+",s))  # me,mee,meee,meeeee,

# s = "meet_asdf_msss_mmns_aaam"  # ? 匹配 0个或1个 [非贪婪匹配]
# print(re.findall("m?",s))

# s = "meet_asdf_msss_mmns_aaam"  # s{3} s重复3次 == sss
# print(re.findall("s{3}",s))

# s = "meet_assdf_msss_mmns_aaam"
# print(re.findall("s{1,3}",s))   # s{1,3} s ss sss
# 指定最少多少次,最多多少次

# a|b # 或
# s = "meet_assdf_msss_mmns_aaam"
# print(re.findall("m|s",s))   # m或者s

# s = "meet_assdf_mssst_(.)mmns_aaamaaatmsssssssssssstt"
# print(re.findall("m(.+)t",s))

# s = "meet_assdf_mssst_(.)mmns_aaamaaatmsssssssssssstt"
# print(re.findall("m(?:..?)t",s))

# s = 'alex_sb ale123_sb wu12sir_sb wusir_sb ritian_sb 的 alex wusir '
# print(re.findall("\w+_sb",s))
# print(re.findall("[a-z]+_sb",s))

# s = '_sb alex 123_sb wu12sir_sb wusir_sb ritian_sb 的 x wusir '
# print(re.search("ale",s).group())
# search 找到1个后就停止查找了,从字符串中进行查找.找到后返回的是一个对象,查看元素.group()

# s = '_sb alex 123_sb wu12sir_sb wusir_sb ritian_sb 的 x wusir '
# print(re.match("ale",s).group())
# match 找到1个后就停止查找了,只从字符串的开头查找.找到后返回的是一个对象,查看元素.group()

# s = '_sb alex,123:sb;wu12sir#sb*wusir!sb ritian_sb 的 x wusir '
# print(re.split("[#,:!*]",s))
# 分割

# 替换
# print(re.sub("barry","太亮",'barry是最好的讲师，barry就是一个普通老师，请不要将barry当男神对待。'))


# compile # 定义匹配规则
# obj = re.compile("\w")
# print(obj.findall("meet_宝元_常鑫垃圾"))

# re.findall("\w","meet_宝元_常鑫大煎饼")


# finditer  # 返回是一个迭代器的地址
# g = re.finditer("\w","常鑫垃圾")
# print(next(g).group())
# for i in g:
#     print(i.group())

# print(re.findall("常(.*?)娃","常鑫垃圾_井盖_烧饼吃娃娃_自行车_葫芦爷爷救娃娃"))
# print(re.findall("常(.*?)娃","常鑫垃圾_井盖_烧饼吃娃娃_自行车_葫芦爷爷救娃娃"))

# print(re.search("(?P<tag_name>\w+)\w+\w+","h1hellh1"))
# print(re.search("(?P<aaa>\w+)dfa","asbsadfasdfa").group("aaa"))
# print(re.search("(?P<cx>\w+)dfa","asbsadfasdfa").group())

# 1 "1-2*(60+(-40.35/5)-(-4*3))"
# 1.1 匹配所有的整数

# s = "1-2*(60+(-40.35/5)-(-4*3))"
# print(re.findall("\d+",s))

# 匹配所有的数字（包含小数）
# print(re.findall("\d+\.\d+|\d+",s))

# 匹配所有的数字（包含小数包含负号）
# print(re.findall("-?\d+\.\d+|-?\d+",s))

# s = "http://blog.csdn.net/make164492212/article/details/51656638"
# print(re.findall("h.*2/",s))

s1 = '''
时间就是1995-04-27,2005-04-27
1999-04-27 老男孩教育创始人
老男孩老师 alex 1980-04-27:1980-04-27
2018-12-08
'''
# print(re.findall("\d+-\d+-\d+",s1))

# 匹配qq号：腾讯从10000开始

# qq = input("请输入QQ号:")
# print(re.findall("[1-9][0-9]{4,9}",qq))

s1 = '''
<div id="cnblogs_post_body" class="blogpost-body"><h3><span style="font-family: 楷体;">python基础篇</span></h3>
<p><span style="font-family: 楷体;">&nbsp; &nbsp;<strong><a href="http://www.cnblogs.com/guobaoyuan/p/6847032.html" target="_blank">python 基础知识</a></strong></span></p>
<p><span style="font-family: 楷体;"><strong>&nbsp; &nbsp;<a href="http://www.cnblogs.com/guobaoyuan/p/6627631.html" target="_blank">python 初始python</a></strong></span></p>
<p><span style="font-family: 楷体;"><strong>&nbsp; &nbsp;<strong><a href="http://www.cnblogs.com/guobaoyuan/articles/7087609.html" target="_blank">python 字符编码</a></strong></strong></span></p>
<p><span style="font-family: 楷体;"><strong><strong>&nbsp; &nbsp;<a href="http://www.cnblogs.com/guobaoyuan/articles/6752157.html" target="_blank">python 类型及变量</a></strong></strong></span></p>
<p><span style="font-family: 楷体;"><strong>&nbsp; &nbsp;<a href="http://www.cnblogs.com/guobaoyuan/p/6847663.html" target="_blank">python 字符串详解</a></strong></span></p>
<p><span style="font-family: 楷体;">&nbsp; &nbsp;<strong><a href="http://www.cnblogs.com/guobaoyuan/p/6850347.html" target="_blank">python 列表详解</a></strong></span></p>
<p><span style="font-family: 楷体;"><strong>&nbsp; &nbsp;<a href="http://www.cnblogs.com/guobaoyuan/p/6850496.html" target="_blank">python 数字元祖</a></strong></span></p>
<p><span style="font-family: 楷体;">&nbsp; &nbsp;<strong><a href="http://www.cnblogs.com/guobaoyuan/p/6851820.html" target="_blank">python 字典详解</a></strong></span></p>
<p><span style="font-family: 楷体;"><strong>&nbsp; &nbsp;<strong><a href="http://www.cnblogs.com/guobaoyuan/p/6852131.html" target="_blank">python 集合详解</a></strong></strong></span></p>
<p><span style="font-family: 楷体;"><strong>&nbsp; &nbsp;<a href="http://www.cnblogs.com/guobaoyuan/articles/7087614.html" target="_blank">python 数据类型</a>&nbsp;</strong></span></p>
<p><span style="font-family: 楷体;"><strong>&nbsp; &nbsp;<a href="http://www.cnblogs.com/guobaoyuan/p/6752169.html" target="_blank">python文件操作</a></strong></span></p>
<p><span style="font-family: 楷体;"><strong>&nbsp; &nbsp;<a href="http://www.cnblogs.com/guobaoyuan/p/8149209.html" target="_blank">python 闭包</a></strong></span></p>
<p><span style="font-family: 楷体;"><strong>&nbsp; &nbsp;<a href="http://www.cnblogs.com/guobaoyuan/articles/6705714.html" target="_blank">python 函数详解</a></strong></span></p>
<p><span style="font-family: 楷体;"><strong>&nbsp; &nbsp;<a href="http://www.cnblogs.com/guobaoyuan/articles/7087616.html" target="_blank">python 函数、装饰器、内置函数</a></strong></span></p>
<p><span style="font-family: 楷体;"><strong>&nbsp; &nbsp;<a href="http://www.cnblogs.com/guobaoyuan/articles/7087629.html" target="_blank">python 迭代器 生成器</a>&nbsp;&nbsp;</strong></span></p>
<p><span style="font-family: 楷体;"><strong>&nbsp; &nbsp;<a href="http://www.cnblogs.com/guobaoyuan/articles/6757215.html" target="_blank">python匿名函数、内置函数</a></strong></span></p>
</div>
'''

# print(re.findall("<span(.*?)>",s1))
# print(re.findall('<a href="(.*?)"',s1))

