# re -- 正则表达式
import re
# re.findall()
# re.search()
# re.match()
# re.sub()
# re.split()
# re.finditer()

# \w
# \d
# ^
# $
# m()t  m(?:)t
# [] [^]
# {}
# | 或者
# \. == 转义成普通的.


# * 0或多
# + 1或多
# ? 0或1
# *? 限制*
# .* 一个任意元素重复出现0次或多次

# m(?P<名字>\w+)t group("名字")