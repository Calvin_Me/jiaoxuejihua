# s = """
# for i in range(10):
#     print(i)
# """

# s1 = """
# def func():
#     print(123)
# func()
# """
# print(eval(s))
# print(exec(s1))  # 牛逼 不能用

# print(hash("asdfas"))

# print(help(list))
# help(dict)


# def func():
#     pass
# print(callable(func))  # 查看是否可调用

# print(float(2))     # 浮点数
# print(complex(56))  # 复数

# print(oct(15))        # 八进制
# print(hex(15))        # 十六进制

# print(divmod(5,2))     # (2, 1) 2商 1余

# print(round(5.3234,2))     # 四舍五入 -- 默认是整数,可以指定保留小数位

# print(pow(2,3))            # 幂
# print(pow(2,3,4))          # 幂,余

# s = "alex"
# print(bytes(s,encoding="utf-8"))

# print(ord("你"))    # 当前编码
# print(chr(20320))

# s = "C:\u3000"
# print(repr(s))

# print("\u3000你好")

# lst = [1,2,3,False,4,5,6,7]
# print(all(lst))   # 判断元素是否都为真  相似and
# print(any(lst))     # 判断元素是否有真    相似or

# name = 1
# def func():
#     a = 123
#     # print(locals())
#     # print(globals())
# func()

# print(globals())   # 全局空间中的变量
# print(locals())   # 查看当前空间的变量