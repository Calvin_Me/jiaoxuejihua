# def func1():
#     print('in func1')
#
# def func2(x):
#     print('in func2')
#     return x
#
# def func3(y):
#     print('in func3')
#     return y
#
# ret = func2(func1)
#
# ret()
# ret2 = func3(func2)
# ret3 = ret2(func1)
# ret3()


# data_list = []  # ['绕不死你']
#
# def func(arg):
#     return data_list.insert(0, arg)
#
# data = func('绕不死你')
# print(data)
# print(data_list)

# item = '老男孩'
# def func():
#     item = 'alex'
#     def inner():
#         print(item)
#     for item in range(10):
#         pass
#     inner()
# func()

# name = '宝元'
# def func():
#     print(name)
#     name = "alex"

# def extendList(val,list=[]):
#     list.append(val)
#     return list
#
# list1 = extendList(10)
# list2 = extendList(123,[])
# list3 = extendList('a')
#
# print('list1=%s'%list1)
# print('list2=%s'%list2)
# print('list3=%s'%list3)


# def extendList(val,list=[]):
#     list.append(val)
#     return list
# print('list1=%s'% extendList(10))# [10]
# print('list2=%s'% extendList(123,[])) #[123]
# print('list3=%s'% extendList('a')) # [10,"a"]

