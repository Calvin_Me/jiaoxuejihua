# 1.生成器
# 生成器的本质就是一个迭代器
# 生成器和迭代器的区别:
    # 迭代器是Python自带的
    # 生成器是咱们(程序员)写得

# 定义一个生成器:

    # 基于函数,
    # 函数体中存在yield就是一个生成器
    # 函数名() 就是产生一个生成器

# 生成器:
#     节省空间 -- 惰性机制
#     不能逆行
#     一次性
#     一个next对应一个yield
#     yield 能够进行返回内容,还能够返回多次
#     yield能够临时停止循环
#     yield 能够记录执行的位置

# yield from -- 将一个可迭代对象的元素逐个返回


# 2.推导式

# list:[变量(加工后的变量) for循环 加工条件]
# dict:{键:值 for循环 加工条件}
# set:{变量(加工后的变量) for循环 加工条件}
# 生成器表达式:(变量(加工后的变量) for循环 加工条件)

# 1.普通循环推导
# 2.筛选推导

# 3.内置函数一
"""
all() any() bytes()
callable()
chr() complex()
divmod() eval()
exec() format()
frozenset() globals()
hash() help() id() input()
int() iter() locals()
next() oct() ord()
pow() repr() round()
"""
# 1.上次考试的卷子重做一遍(不能参考)  #周六晚上发答案!