



# 昨日内容回顾

### jquery引入

```
下载链接：jQuery官网  https://jquery.com/
中文文档：jQuery AP中文文档

<script src="jquery.js"></script>
<script>
</script>

第二种方式,网络地址引入
<!--<script src="https://cdn.bootcss.com/jquery/3.4.1/jquery.js"></script>-->

```

### 下载

![1568768586542](C:\Users\Administrator\Desktop\assets\1568768586542.png)

![1568768603513](C:\Users\Administrator\Desktop\assets\1568768603513.png)



```
jquey方法找到的标签对象称为jquery对象
原生js找到的标签对象称之为DOM对象
dom对象只能调用dom对象的方法,jquery对象只能用jquery方法,不能互通
```



jquery对象和dom对象互相转换

```
/jQuery对象转成DOM对象，通过一个jQuery对象+[0]索引零，就变成了DOM对象，就可以使用JS的代码方法了，DOM对象转换成jQuery对象：$(DOM对象)，通过$符号包裹一下就可以了
jquery对象 转换成 dom对象 :  $('#d1')[0] -- dom对象
	$('.c1')[2]  通过[索引]的方法从jquery中获取的取对象,全部是DOM对象
	
dom对象转换为jquery对象 :  $(dom对象) -- jquery对象

```



## 标签查找

### 基础选择器

```
　　　　基本选择器（同css）
　　　　　　id选择器：

			$("#id")  #不管找什么标签，用什么选择器，都必须要写$("")，引号里面再写选择器，通过jQuery找到的标签对象就是一个jQuery对象，用原生JS找到的标签对象叫做DOM对象，看我们上面的jQuery对象部分的内容
　　　　　　标签选择器：$("tagName")
　　　　　　class选择器：$(".className")
配合使用：$("div.c1")  // 找到有c1 class类的div标签
　　　　　　所有元素选择器：$("*")
　　　　　　组合选择器$("#id, .className, tagName")
　　　　

```

### 层级选择器：（同css）

```
x和y可以为任意选择器

$("x y");// x的所有后代y（子子孙孙）
$("x > y");// x的所有儿子y（儿子）
$("x + y")// 找到所有紧挨在x后面的y
$("x ~ y")// x之后所有的兄弟y
```

### 基本筛选器

```
:first // 第一个
:last // 最后一个
:eq(index)// 索引等于index的那个元素
:even // 匹配所有索引值为偶数的元素，从 0 开始计数
:odd // 匹配所有索引值为奇数的元素，从 0 开始计数
:gt(index)// 匹配所有大于给定索引值的元素
:lt(index)// 匹配所有小于给定索引值的元素
:not(元素选择器)// 移除所有满足not条件的标签
:has(元素选择器)// 选取所有包含一个或多个标签在其内的标签(指的是从后代元素找)
示例写法:
	$('li:has(span)');
	
$("div:has(h1)")// 找到所有后代中有h1标签的div标签，意思是首先找到所有div标签，把这些div标签的后代中有h1的div标签筛选出来
$("div:has(.c1)")// 找到所有后代中有c1样式类（类属性class='c1'）的div标签
$("li:not(.c1)")// 找到所有不包含c1样式类的li标签
$("li:not(:has(a))")// 找到所有后代中不含a标签的li标签
```



简单绑定事件的示例

```
// 绑定事件的方法
$('#btn').click(function () {
    $('.mode')[0].classList.remove('hide');
    $('.shadow')[0].classList.remove('hide');
	jquery写法:
	$('.mode,.shadow').removeClass('hide');
})
```

### 属性选择器

```
[attribute]
[attribute=value]// 属性等于
[attribute!=value]// 属性不等于
示例:
	$('[title]')

// 示例,多用于input标签
<input type="text">
<input type="password">
<input type="checkbox">
$("input[type='checkbox']");// 取到checkbox类型的input标签
$("input[type!='text']");// 取到类型不是text的input标签
```



### 表单筛选器

```
:text
:password
:file
:radio
:checkbox

:submit
:reset
:button
简单示例:
	
    <div>
        用户名: <input type="text">
    </div>

    <div>
        密码: <input type="password">
    </div>

    <div>
        sex:
        <input type="radio" name="sex">男
        <input type="radio" name="sex">女
        <input type="radio" name="sex">不详
    </div>
    
    找到type为text的input标签:$(':text')
    
```



### 表单对象属性

```
:enabled
:disabled
:checked
:selected

示例:
	<div>
        用户名: <input type="text">
    </div>

    <div>
        密码: <input type="password" disabled>
    </div>
	
    <div>
        sex:
        <input type="radio" name="sex">男
        <input type="radio" name="sex">女
        <input type="radio" name="sex">不详
    </div>
    <select name="" id="">

        <option value="1">怼姐</option>
        <option value="2">珊姐</option>
        <option value="3">华丽</option>
        <option value="4">亚索</option>

    </select>
    操作:
    找到可以用的标签 -- $(':enabled')
    找select标签被选中的option标签  $(':selected')
```

### 链式表达式

```
<ul>
    <li>陈硕</li>
    <li>子文</li>
    <li class="c1">李业</li>
    <li>吕卫贺</li>
    <li>峡谷先锋珊姐</li>
    <li class="c2">怼姐</li>
    <li>骚强</li>

</ul>

操作		$('li:first').next().css('color','green').next().css('color','red');
```



筛选器方法

```
下一个
    $("#id").next()
    $("#id").nextAll()
    $("#id").nextUntil("#i2") #直到找到id为i2的标签就结束查找，不包含它
上一个
	$("#id").prev()
    $("#id").prevAll()
    $("#id").prevUntil("#i2")

<ul>
    <li>陈硕</li>
    <li>子文</li>
    <li class="c1">李业</li>
    <li>吕卫贺</li>
    <li>峡谷先锋珊姐</li>
    <li class="c2">怼姐</li>
    <li>骚强</li>

</ul>

示例:
	$('li:first').next()  找到第一个标签的下一个标签
	$('.c2').prevUntil('.c1');

```

```
父元素
	$("#id").parent()
    $("#id").parents()  // 查找当前元素的所有的父辈元素（爷爷辈、祖先辈都找到）
    $("#id").parentsUntil('body') // 查找当前元素的所有的父辈元素，直到遇到匹配的那个元素为止，这里直到body标签，不包含body标签，基本选择器都可以放到这里面使用。
    
儿子和兄弟元素：
    $("#id").children();// 儿子们
    $("#id").siblings();// 兄弟们，不包含自己，.siblings('#id')，可以在添加选择器进行进一步筛选
```

```
查找find,找的是后代
	$("div").find("p")  -- 等价于$("div p")
筛选filter
	$("div").filter(".c1") -- 等价于 $("div.c1")  // 从结果集中过滤出有c1样式类的，从所有的div标签中过滤出有class='c1'属性的div，和find不同，find是找div标签的子子孙孙中找到一个符合条件的标签
```

```
.first() // 获取匹配的第一个元素
.last() // 获取匹配的最后一个元素
.not() // 从匹配元素的集合中删除与指定表达式匹配的元素
.has() // 保留包含特定后代的元素，去掉那些不含有指定后代的元素。
.eq() // 索引值等于指定值的元素
```





## 操作标签

### 样式操作

```
样式类操作
	addClass();// 添加指定的CSS类名。
    removeClass();// 移除指定的CSS类名。
    hasClass();// 判断样式存不存在
    toggleClass();// 切换CSS类名，如果有就移除，如果没有就添加。
```

### css操作

```
单个方式:$('div').css('background-color','green');
多个方式:$('div').css({'background-color':'yellow','width':'400px'});
```

### 位置操作

```
offset()// 获取匹配元素在当前窗口的相对偏移或设置元素位置
position()// 获取匹配元素相对父元素的偏移，不能设置位置

	.offset()方法允许我们检索一个元素相对于文档（document）的当前位置。和 .position()的差别在于： .position()获取相对于它最近的具有相对位置(position:relative或position:absolute)的父级元素的距离，如果找不到这样的元素，则返回相对于浏览器的距离

示例:
	$('.c2').offset(); 查看位置
	$('.c2').offset({top:100,left:200}); 设置位置
	
	
	代码:
	<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Title</title>

        <style>
            /*body{*/
            /*    margin: 0;*/
            /*}*/
            .c1{
                background-color: red;
                height: 100px;
                width: 100px;
                display: inline-block;

            }
            .c2{
                background-color: green;
                height: 100px;
                width: 100px;
                display: inline-block;
            }
        </style>
    </head>
    <body>
    <div class="cc">
        <div class="c1"></div><div class="c2"></div>
    </div>
    <script src="jquey.js"></script>
    </body>
    </html>
```

```
$(window).scrollTop()  //滚轮向下移动的距离
$(window).scrollLeft() //滚轮向右移动的距离
```



### 尺寸

```
height() //盒子模型content的大小，就是我们设置的标签的高度和宽度
width()
innerHeight() //内容content高度 + 两个padding的高度
innerWidth()
outerHeight() //内容高度 + 两个padding的高度 + 两个border的高度，不包括margin的高度，因为margin不是标签的，是标签和标签之间的距离
outerWidth()
示例:
	<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Title</title>

        <style>
            .c1{
                width: 100px;
                height: 100px;
                padding: 20px 30px;
                border: 2px solid red;

            }



        </style>

    </head>
    <body>

    <div class="c1"></div>


    </body>
    <script src="jquery.js"></script>
    </html>
    
    操作:
    	$('.c1').height();
        $('.c1').width();
        $('.c1').innerWidth();
        $('.c1').outerWidth();
```

### 文本操作

```
html()// 取得第一个匹配元素的html内容，包含标签内容
html(val)// 设置所有匹配元素的html内容，识别标签，能够表现出标签的效果
text()// 取得所有匹配元素的内容，只有文本内容，没有标签
text(val)// 设置所有匹配元素的内容，不识别标签，将标签作为文本插入进去
示例:
	取值	
		$('.c1').html();
		$('.c1').text();
	设置值
        $('.c1').text('<a href="">百度</a>');
        $('.c1').html('<a href="">百度</a>');
```





# 今日内容

## 标签操作

### 值操作

```
取值:
	文本输入框:$('#username').val();
	input,type=radio单选框: $('[type="radio"]:checked').val();,首先找到被选中的标签,再进行取值
	input,type=checkbox多选框: 通过val方法不能直接获取多选的值,只能拿到一个,想拿到多个项的值,需要循环取值
		var d = $('[type="checkbox"]:checked');
		for (var i=0;i<d.length;i++){
			console.log(d.eq(i).val());
		}
	单选下拉框select: -- $('#s1').val();
	多选下拉框select: -- $('#s2').val(); -- ['1','2']

设置值
	文本输入框: -- $('#username').val('xxx');
	input,type=radio单选框: -- $(':radio').val(['1']) 找到所有的radio,然后通过val设置值,达到一个选中的效果.
	给单选或者多选框设置值的时候,只要val方法中的值和标签的value属性对应的值相同时,那么这个标签就会被选中.
	此处有坑:$(':radio').val('1');这样设置值,不带中括号的,意思是将所有的input,type=radio的标签的value属性的值设置为1.
	
	input,type=checkbox多选框: -- $(':checkbox').val(['1','2']);
	
	单选下拉框select: -- $('#s1').val(['3']);
	多选下拉框select: -- $('#s2').val(['1','2']);
	
	统一一个方法:
		选择框都用中括号设置值.
```



作业1思路

```
绑定点击事件
1 获取input标签中的值val
2 val().trim().length 
3 =0  
	方式1:提前在input标签后面放一个span标签,加一个类值{color:red},找到span并添加文本内容, .text() .html()
4 如果不等于0,清空span中的内容

```



### 属性操作

```

设置属性: -- $('#d1').attr({'age1':'18','age2':'19'});
		单个设置:$('#d1').attr('age1','18');
查看属性值: -- $('#d1').attr('age1');
删除属性: -- $('#d1').removeAttr('age1'); 括号里面写属性名称
```



```
prop和attr方法的区别:
总结一下：
	1.对于标签上有的能看到的属性和自定义属性都用attr
	2.对于返回布尔值的比如checkbox、radio和option的是否被选中或者设置其被选中与取消选中都用prop。
	具有 true 和 false 两个属性的属性，如 checked, selected 或者 disabled 使用prop()，其他的使用 attr()
	checked示例:
		attr():
            查看值,checked 选中--'checked'  没选中--undefined
                $('#nv').attr({'checked':'checked'}); 
            设置值,attr无法完成取消选中
                $('#nv').attr({'checked':'undefined'});
                $('#nv').attr({'checked':undefined});
                
         prop():
         	查看值,checked 选中--true  没选中--false
         		$(':checkbox').prop('checked');
         	设置值:
         		$(':checkbox').prop('checked',true);
         		$(':checkbox').prop('checked',false);
```



### 文档处理

```

姿势1:添加到指定元素内部的后面
    $(A).append(B)// 把B追加到A
    $(A).appendTo(B)// 把A追加到B
    
	append示例:
        方式1: 
            创建标签
                var a = document.createElement('a');
                $(a).text('百度');
                $(a).attr('href','http://www.baidu.com');
                $('#d1').append(a);
        方式2:(重点)
            $('#d1').append('<a href="xx">京东</a>');
	appendto示例
		$(a).appendTo('#d1');
		
姿势2:添加到指定元素内部的前面
	$(A).prepend(B)// 把B前置到A
	$(A).prependTo(B)// 把A前置到B
姿势3:添加到指定元素外部的后面
	$(A).after(B)// 把B放到A的后面
	$(A).insertAfter(B)// 把A放到B的后面
姿势4:
	$(A).before(B)// 把B放到A的前面
	$(A).insertBefore(B)// 把A放到B的前面

移除和清空元素
	remove()// 从DOM中删除所有匹配的元素。
	empty()// 删除匹配的元素集合中所有的子节点，包括文本被全部删除，但是匹配的元素还在
	示例:
		$('#d1').remove();
		$('#d1').empty();
		
替换:
	replaceWith()
	replaceAll()
     示例:
     	$('#d1').replaceWith(a);  用a替换前面的标签
     	$(a).replaceAll('#d1');   
     
```



克隆(复制标签)

```
示例:
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>

	<button class="btn">屠龙宝刀,点击就送!</button>

</body>
<script src="jquery.js"></script>
<script>
    $('.btn').click(function () {
        // var btnEle = $(this).clone(); // 不带参数的克隆不能克隆绑定的事件
        var btnEle = $(this).clone(true); // 参数写个true,就能复制事件
        $(this).after(btnEle);

    })

</script>


</html>
```



作业3

```
1 模态对话框结合点击事件完成弹出和隐藏
2 点击取消,关闭对话框
3 点击确认,关闭对话框,获取用户输入的内容
4 拼接一个tr标签,将数据放到里面的td标签里面,然后将tr标签放到tbody标签内部的后面

5 开除,点击这一行的那个开除按钮,就将本行tr标签删除 remove方法
```



### 事件

```
绑定事件的两种方式:
	// 绑定事件的方式1
    // $("#d1").click(function () {
    //     $(this).css('background-color','green');
    // })

    // 方式2
    $('#d1').on('click',function () {
        $(this).css('background-color','green');
    })
```



常用事件



```

click(function(){...})
hover(function(){...})
blur(function(){...})
focus(function(){...})
change(function(){...}) //内容发生变化，input，select等
keyup(function(){...})  
mouseover 和 mouseenter的区别是：mouseover事件是如果该标签有子标签，那么移动到该标签或者移动到子标签时会连续触发，mmouseenter事件不管有没有子标签都只触发一次，表示鼠标进入这个对象

```

```
示例:
	<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Title</title>
        <style>
            #d1{
                background-color: red;
                height: 200px;
                width: 200px;
            }
            #d2{
                background-color: green;
                height: 200px;
                width: 200px;
            }

            .dd1{
                background-color: yellow;
                height: 40px;
                width: 40px;
            }
            .dd2{
                background-color: pink;
                height: 40px;
                width: 40px;
            }

        </style>
    </head>
    <body>



    <div id="d1">
        <div class="dd1"></div>
    </div>
    <div id="d2">
        <div class="dd2"></div>
    </div>

    用户名:<input type="text" id="username">


    <br>

    <!--<select name="" id="s1">-->
    <!--    <option value="1">上海</option>-->
    <!--    <option value="2">深圳</option>-->
    <!--    <option value="3">贵州</option>-->
    <!--</select>-->

    <input type="text" id="xxx">

    </body>
    <script src="jquery.js"></script>
    <script>
        // 绑定事件的方式1
        // $("#d1").click(function () {
        //     $(this).css('background-color','green');
        // })

        // 方式2
        // $('#d1').on('click',function () {
        //     $(this).css('background-color','green');
        // });
        //
        // // 获取光标触发的事件
        // $('#username').focus(function () {
        //     $(this).css('background-color','green');
        // });
        // // 失去光标触发的事件
        // $('#username').blur(function () {
        //     $(this).css('background-color','white');
        // });
        // // 域内容发生变化触发的事件,一般用于select标签
        // $('#s1').change(function () {
        //      // $('#d1').css('background-color','black');
        //     console.log('xxxxx')
        //
        // });

        // $('#xxx').change(function () {
        //     console.log($(this).val());
        // })

        // 输入内容实时触发的事件,input事件只能on绑定
        // $('#xxx').on('input',function () {
        //     console.log($(this).val());
        // });
        //
        // //绑定多个事件 事件名称空格间隔
        // $('#xxx').on('input blur',function () {
        //     console.log($(this).val());
        // })


        // 鼠标进入触发的事件
        // $('#d1').mouseenter(function () {
        //     $(this).css('background-color','green');
        // });
        // // 鼠标离开触发的事件
        // $('#d1').mouseout(function () {
        //     $(this).css('background-color','red');
        // });

        // hover事件 鼠标进进出出的事件
        // $('#d1').hover(
        //     // 鼠标进入
        //     function () {
        //         $(this).css('background-color','green');
        //     },
        //     // 鼠标离开
        //     function () {
        //         $(this).css('background-color','red');
        //     }
        // );


        $('#d1').mouseenter(function () {
            console.log('xxx');
        });
        $('#d2').mouseover(function () {
            console.log('ooo');
        });
		// 键盘按下
		   // $(window).keydown(function (e) {
            //     console.log(e.keyCode);
            //
            // });
         // 键盘抬起
            $(window).keyup(function (e) {
                console.log(e.keyCode);
            });

    </script>
    </html>
```



### 移除事件（不常用）

```
.off( events [, selector ][,function(){}])
off() 方法移除用 .on()绑定的事件处理程序。

$("li").off("click")；就可以了
```



### 事件冒泡

```
    // 事件冒泡,子标签和父标签(祖先标签)绑定了相同的事件,比如点击事件,那么当你点击子标签时,会一层一层的往上触发父级或者祖父级等等的事件
    $('.c1').click(function () {
        alert('父级标签!!!');

    });
    $('.c2').click(function (e) {
        alert('子标签~~~');
        // 阻止事件冒泡(阻止事件发生)
        return false; //方式1
        // e.stopPropagation() // 方式2
    })
```

### 事件委托

```

	<div id="d1">
    	<button class="btn">屠龙宝刀,点击就送!</button>.
    	
	</div>

    // 事件委托
    $('#d1').on('click','.btn',function () {
          // $(this)是你点击的儿子标签
        var a= $(this).clone();
        $('#d1').append(a);
    });
//中间的参数是个选择器，前面这个$('table')是父级标签选择器，选择的是父级标签，意思就是将子标签（子子孙孙）的点击事件委托给了父级标签
//但是这里注意一点，你console.log(this)；你会发现this还是触发事件的那个子标签，这个记住昂
```



























































































