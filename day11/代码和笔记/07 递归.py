# 递归
# 1.自己调用自己 (不断调用自己本身) -- 死递归
# 2.有明确的终止条件
# 满足以上两个才是有效递归

# 递:一直执行直到碰到结束条件
# 归:从结束条件开始往回退

# 官方声明: 最大层次1000,实际测试 998/997

# def func():
#     print(123)
#     func()
# func()

# def age(n): # 1,2,3
#     if n == 3:
#         return "猜对了"
#     else:
#         age(n+1)
# print(age(1))

# def age2(n):
#     if n == 3:
#         return "猜对了"
#
# def age1(n):
#     if n == 3:
#         return "猜对了"
#     else:
#         age2(n+1)
#
# def age(n):
#     if n == 3:
#         return "猜对了"
#     else:
#         age1(n+1)
# age(1)


# 1.宝元  18-2-2-2
# 2.太白  18-2-2
# 3.wusir 18-2
# 4.alex  18

# def age(n):
#     if n == 4:
#         return 18
#     else:
#         return age(n+1)-2
# print(age(1))


# def age4(n):
#     if n == 4:
#         return 18
# def age3(n):
#     if n == 4: # 问的是不是第四个了
#         return 18
#     else:
#         return age4(n+1)-2
# def age2(n):
#     if n == 4:  # 问的是不是第四个了
#         return 18
#     else:
#         return age3(n+1)-2
# def age1(n):
#     if n == 4: # 问的是不是第四个了
#         return 18
#     else:
#         return age2(n+1)-2
# print(age1(1))

#(自己学习的方法, -- 学会百度[学会自学] -- 先同学讨论)