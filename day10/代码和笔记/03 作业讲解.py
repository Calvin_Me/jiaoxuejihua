"""
2.写函数，检查获取传入列表或元组对象的所有奇数位索引对应的元素，并将其作为新列表返回给调用者。
"""
# lst = [1,2,3,4,5]
# def func(a):
#     new_lst = []
#     for i in range(len(a)):
#         if i % 2 == 1:
#             new_lst.append(a[i])
#     return new_lst
#
# print(func(lst))

"""
3.写函数，判断用户传入的对象（字符串、列表、元组）长度是否大于5。
"""
# lst = [1,2,3,4,5,6,7,8]
# def func(b):
#     if len(b) > 5:
#         print("大于5")
#     else:
#         print("不是大于")
# func(lst)

# lst = [1,2,3]
# def func(b):
#     return "大于5" if len(b) > 5 else "不大于"
# print(func(lst))

# 三元运算符:
# 变量 = 条件成立的结果  条件判断  条件不成立的结果

"""
4.写函数，检查传入列表的长度，如果大于2，那么仅保留前两个长度的内容，并将新内容返回给调用者。
"""
# lst = [1, 2, 3, 4, 5, 6]
# def func(l):
#     if len(l) > 2:
#         return l[:2]
# print(func(lst))  # 调用者就是函数名+()

"""
5.写函数，计算传入函数的字符串中,[数字]、[字母] 以及 [其他]的个数，并返回结果。
"""

# s = "alexdsb1234,[123,345],行"
# def func(s):
#     num_sum = 0
#     str_sum = 0
#     else_sum = 0
#     for i in s:
#         if i.isdecimal():
#             num_sum += 1
#         elif i.isalpha():
#             str_sum += 1
#         else:
#             else_sum += 1
#
#     return f"数字:{num_sum},字母:{str_sum},其他:{else_sum}"
#
# print(func(s))

"""
6.写函数，接收两个数字参数，返回比较大的那个数字。
"""

# def func(a,b):
#     if a > b:
#         return a
#     else:
#         return b

# def func(a,b):
#     return a if a > b else b
# print(func(7,7))

"""
7.写函数，检查传入字典的每一个value的长度,如果大于2，
那么仅保留前两个长度的内容，并将新内容返回给调用者。
dic = {"k1": "v1v1", "k2": [11,22,33,44]}
PS:字典中的value只能是字符串或列表
"""
# dic = {"k1": "v1v1", "k2": [11,22,33,44]}
# def func(d):
#     for k,v in d.items():
#         if len(v) > 2:
#             d[k] = v[:2]
#     return d
# print(func(dic))


"""
8.写函数，此函数只接收一个参数这个参数必须是列表数据类型，
此函数完成的功能是返回给调用者一个字典，
此字典的键值对为列表的索引及对应的元素。
例如传入的列表为：[11,22,33] 返回的字典为 {0:11,1:22,2:33}。
"""
# lst = [11, 22, 33]
# def func(args:list):
#     dic = {}
#     for i in range(len(args)):
#         dic[i] = args[i]
#     return dic
# print(func(lst))

# lst = [11, 22, 33]
# def func(d):
#     # enumerate 枚举
#     dic = {}
#     for k, v in enumerate(d):
#         dic[k] = v
#     return dic
# print(func(lst))

"""
9.写函数，函数接收四个参数分别是：姓名，性别，年龄，学历。
用户通过输入这四个内容，然后将这四个内容传入到函数中，
此函数接收到这四个内容，将内容追加到一个student_msg文件中。
"""
# name = input("姓名:")
# sex = input("性别:")
# age = input("年龄:")
# level = input("学历:")
#
# def func(name,sex,age,level):
#     with open("student_msg","a",encoding="utf-8")as f:
#         f.write(f"姓名:{name},性别:{sex},年龄:{age},学历:{level}")
# func(name,sex,age,level)


# 10
while True:
    print("Q/退出")
    name = input("姓名:")
    if name.upper() == "Q":
        break
    sex = input("性别:")
    age = input("年龄:")
    level = input("学历:")

    def func(name,age,level,sex="男"):
        with open("student_msg","a",encoding="utf-8")as f:
            f.write(f"姓名:{name},性别:{sex},年龄:{age},学历:{level}\n")
    if sex == "女":
        func(name,age,level,sex)

    else:
        func(name,age,level)