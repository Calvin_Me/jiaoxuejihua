
# def func():
#     global a  # 声明修改全局变量的值
#     a += 1
#     print(a)
# func()
# print(a)

# a = 10
# def f1():
#     a = 10
#     def f2():
#         a = 15
#         def f3():
#             global a
#             a += 1
#             print(a)  # 11
#         print(a)  # 15
#         f3()
#     print(a)  # 10
#     f2()
# f1()

# a = 10
# def func():
#     def f1():
#         global a
#         a += 1
#         def foo():
#             nonlocal a
#             a += 1
#             print(a)   # 31
#         foo()
#         print(a) # 31
#     f1()
# func()
# print(a)   # 10

# global : 修改全局空间的变量对应的值
# nonlocal :在外层的函数中,修改局部空间的变量值.完全不涉及全局变量,
# 只修改离它最近的一层,最近的一层没有变量继续向上找,直到找到最外层函数