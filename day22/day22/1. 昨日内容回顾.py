class GameRole:

    game = 'LOL'

    def __init__(self, name, age):
        self.name = name
        self.age = age

    def func(self):
        print(self.name)
        print('in func')

# 类名的角度:
# 类名操作静态属性
# GameRole.a = 6
# del
# GameRole.game = 'DNF'
# print(GameRole.game)
# print(GameRole.__dict__)

# GameRole.func(111)


# 对象
# obj = GameRole('戴青', 18)


'''
开辟一个对象空间
自动执行__init__方法,并将对象空间传给self.
运行__init__方法,给对象封装属性.
'''
# 对象调用对象的属性
# __dict__
# obj = GameRole('戴青', 18)
# print(obj.__dict__)
# print(obj.name)

# 对象调用类的属性
# print(obj.game)
# 对象调用类的方法
# obj.func()
# GameRole.func(obj)

# self
# 一个类可以实例化多个对象
# obj = GameRole('戴青', 18)
# obj1 = GameRole('戴青', 18)


class GameRole:

    def __init__(self, name, ad, hp):
        self.name = name
        self.ad = ad
        self.hp = hp

    def attack(self, p1):
        p1.hp = p1.hp - self.ad
        print(f"{self.name}攻击{p1.name},谁掉了{self.ad}血,  还剩{p1.hp}血")
        print(f'{p1.name}的血量{p1.hp}')


gailun = GameRole('盖伦', 10, 100)
yasuo = GameRole('剑豪', 20, 80)
gailun.attack(yasuo)