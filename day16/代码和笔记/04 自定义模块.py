# 1. 自定义一个模块
# import # 导入 (拿工具箱)

# 模块分类:
#     1.内置模块(标准库) -- python解释器自带的.py文件(模块)
#     2.第三方模块(各种大神写的) -- 需要额外下载的 (并发编程开始讲解) (pypi)
#     3.自定义模块(自己写的) -- 不需要额外下载

# 模块:
#     分模块的好处:
#       1.避免写重复代码
#       2.可以多次利用
#       3.拿来主义


# import test
# test.func()

# 2.导入发生的事情
# 2.1 当前的名称空间中开辟一个新的空间(test)
# 2.2 将模块中所有的代码执行
# 2.3 通过模块名.进行查找函数(工具)

# print(locals())
# import test
# print(locals())

# import test
# print(test.name)
# print(test.func())


#错误的示例:
# import test.py
# # print(test.py.func())

# import test


# import test
# import test
# import test
# import test
# import test
# print(test.name)

# import test as t
# print(t.name)


# 1.使用别名能够使文件名更短
msg = """
1.扳手
2.螺丝刀
>>>
"""
# 做一兼容性
# choose = input(msg)
# if choose == "1":
#     import meet
#     meet.func()
# elif choose == "2":
#     import test
#     test.func()



# # 做一兼容性
# choose = input(msg)
# if choose == "1":
#     import meet as t
# elif choose == "2":
#     import test as t
#
# t.func()


# import test  # 把工具箱拿过来

# from test import func
# func()

# import 和 from 的区别
# import # 把工具箱拿过来
    # 缺点:占用内存比较大
    # 优点:不会和当前文件定义的变量或者函数发生冲突

        # import test
        # name = "宝元"
        # print(test.name)
        # print(name)

# from :
#     缺点:会和当前文件定义的变量或者函数发生冲突

        # name = "宝元"
        # from test import name
        # print(name)

        # 解决方法:
            # name = "宝元"
            # from test import name as n
            # print(name)
            # print(n)

#     优点:占用内存比较小

# name = "宝元"
# def func():
#     print("is 马桶推送器")

# from test import *
# print(name)
# func()

# from test import * 会出现覆盖的现象,不能解决
# 飘红不一定是报错

# from meet import *
# print(func)
# print(name)
#
# __all__ = ["可以被导入的函数名和变量名"]

# from meet import foo
# import meet
# print(meet.foo)

# __all__ = ["可以被导入的函数名和变量名"]

# 模块导入时的坑
# name = "宝元"
# import test
# print(test.name)


# 模块的两种用法:
#     1.脚本(在cmd中执行 python test.py)
#     2.模块(不使用或者导入)

# from meet import *
# func()

# __name__ == meet

# from test import *
# if __name__ == '__main__': # 测试接口
#     func()

# 在当前模块中使用__name__ 就是 "__main__"
# 当模块被导入的时候__name__就是被导入的模块名

# 导入路径:
# import meet
# print(meet.name)

# 使用相对路径:
# from day15.t1 import meet
# print(meet.name)

# 使用绝对路径:
# 错误示例:
    # from r"D:\" import meet
    # from ../

# 正确的绝对路径:
# from sys import path
# path.insert(0,"D:\\")
# import meet
# print(meet.name)

# 自定义 > 内置 > 第三方