# MySQL内容回顾

数据库

DBMS  

mysql  -RDBMS 关系型

数据库分类

```
关系型:mysql\oracle\sqlserver\access
非关系型:redis,mongodb...
```

修改密码:

```
mysql>set password for root@'127.0.0.1' =password('666')
mysqladmin -uroot -p老密码 password 新密码;
mysql>update user set password=password('66') where user='root';
```

库操作

```
创建库:
	create database 库名 charset='utf8';
连接库
	use 库名
查询库
	show databases;
	show create database 库名  (查看库的详细创建语句)
删库
	drop database 库名;


```

表操作

```
创建:
	create table 表名(
		字段1名 类型(宽度) 约束条件,
		字段2名 类型(宽度) 约束条件
	);

删除
	drop table 表名;
查看表
	show create table 表名;  #表信息竖向展示
	show create table 表名\G
	show tables;
	select * from userinfo\G  #表信息竖向展示

清空表
	truncate 表名;    清空表,自增重置
	delete from 表名; 清空表

```

 存储引擎

```

可以成为表类型
	innodb和myisam
	MyISAM引擎特点：
        1.不支持事务
            事务是指逻辑上的一组操作，组成这组操作的各个单元，要么全成功要么全失败。
        2.表级锁定
            数据更新时锁定整个表：其锁定机制是表级锁定，也就是对表中的一个数据进行操作都会将这个表锁定，其他人不能操作这个表，这虽然可以让锁定的实现成本很小但是也同时大大降低了其并发性能。
        3.读写互相阻塞
            不仅会在写入的时候阻塞读取，MyISAM还会再读取的时候阻塞写入，但读本身并不会阻塞另外的读。
        4.只会缓存索引
            MyISAM可以通过key_buffer_size的值来提高缓存索引，以大大提高访问性能减少磁盘IO，但是这个缓存区只会缓存索引，而不会缓存数据。
        
        5.读取速度较快
            占用资源相对较少
        6.不支持外键约束，但只是全文索引
        7.MyISAM引擎是MySQL5.5版本之前的默认引擎，是对最初的ISAM引擎优化的产物。
	
	innodb
	InnoDB引擎
        介绍：InnoDB引擎是MySQL数据库的另一个重要的存储引擎，正称为目前MySQL AB所发行新版的标准，被包含在所有二进制安装包里。和其他的存储引擎相比，InnoDB引擎的优点是支持兼容ACID的事务(类似于PostGreSQL)，以及参数完整性(即对外键的支持)。Oracle公司与2005年10月收购了Innobase。Innobase采用双认证授权。它使用GNU发行，也允许其他想将InnoDB结合到商业软件的团体获得授权。

InnoDB引擎特点：
        1.支持事务：支持4个事务隔离界别，支持多版本读。
        2.行级锁定(更新时一般是锁定当前行)：通过索引实现，全表扫描仍然会是表锁，注意间隙锁的影响。
        3.读写阻塞与事务隔离级别相关(有多个级别，这就不介绍啦~)。
        4.具体非常高效的缓存特性：能缓存索引，也能缓存数据。
        5.整个表和主键与Cluster方式存储，组成一颗平衡树。(了解)
        6.所有SecondaryIndex都会保存主键信息。(了解)
        7.支持分区，表空间，类似oracle数据库。
        8.支持外键约束，不支持全文索引(5.5之前)，以后的都支持了。
        9.和MyISAM引擎比较，InnoDB对硬件资源要求还是比较高的。

```

使用场景

```
MyISAM引擎适用的生产业务场景
        1.不需要事务支持的业务(例如转账就不行，充值也不行)
        2.一般为读数据比较多的应用，读写都频繁场景不适合，读多或者写多的都适合。
        3.读写并发访问都相对较低的业务(纯读纯写高并发也可以)(锁定机制问题)
        4.数据修改相对较少的业务(阻塞问题)
        5.以读为主的业务，例如：www.blog,图片信息数据库，用户数据库，商品库等业务
        6.对数据一致性要求不是很高的业务。
        7.中小型的网站部分业务会用。
        小结：单一对数据库的操作都可以示用MyISAM，所谓单一就是尽量纯读，或纯写(insert,update,delete)等。
        
InnoDB引擎
        介绍：InnoDB引擎是MySQL数据库的另一个重要的存储引擎，正称为目前MySQL AB所发行新版的标准，被包含在所有二进制安装包里。和其他的存储引擎相比，InnoDB引擎的优点是支持兼容ACID的事务(类似于PostGreSQL)，以及参数完整性(即对外键的支持)。Oracle公司与2005年10月收购了Innobase。Innobase采用双认证授权。它使用GNU发行，也允许其他想将InnoDB结合到商业软件的团体获得授权。

InnoDB引擎特点：
        1.支持事务：支持4个事务隔离界别，支持多版本读。
        2.行级锁定(更新时一般是锁定当前行)：通过索引实现，全表扫描仍然会是表锁，注意间隙锁的影响。
        3.读写阻塞与事务隔离级别相关(有多个级别，这就不介绍啦~)。
        4.具体非常高效的缓存特性：能缓存索引，也能缓存数据。
        5.整个表和主键与Cluster方式存储，组成一颗平衡树。(了解)
        6.所有SecondaryIndex都会保存主键信息。(了解)
        7.支持分区，表空间，类似oracle数据库。
        8.支持外键约束，不支持全文索引(5.5之前)，以后的都支持了。
        9.和MyISAM引擎比较，InnoDB对硬件资源要求还是比较高的。
         
        
```

事务:

```
事务介绍：
        简单地说，事务就是指逻辑上的一组SQL语句操作，组成这组操作的各个SQL语句，执行时要么全成功要么全失败。
        例如：你给我转账5块钱，流程如下
            a.从你银行卡取出5块钱，剩余计算money-5
            b.把上面5块钱打入我的账户上，我收到5块，剩余计算money+5.
        上述转账的过程，对应的sql语句为：
                update 你_account set money=money-5 where name='你'；
                update 我_account set money=money+5 where name='我'；
        上述的两条SQL操作，在事务中的操作就是要么都执行，要么都不执行，不然钱就对不上了。
        这就是事务的原子性(Atomicity)。
    事务的四大特性：
        1.原子性(Atomicity)
            事务是一个不可分割的单位，事务中的所有SQL等操作要么都发生，要么都不发生。
        2.一致性(Consistency)
            事务发生前和发生后，数据的完整性必须保持一致。
        3.隔离性(Isolation)
            当并发访问数据库时，一个正在执行的事务在执行完毕前，对于其他的会话是不可见的，多个并发事务之间的数据是相互隔离的。也就是其他人的操作在这个事务的执行过程中是看不到这个事务的执行结果的，也就是他们拿到的是这个事务执行之前的内容，等这个事务执行完才能拿到新的数据。
        4.持久性(Durability)
            一个事务一旦被提交，它对数据库中的数据改变就是永久性的。如果出了错误，事务也不允撤销，只能通过'补偿性事务'。
        
    事务的开启：
        数据库默认事务是自动提交的，也就是发一条sql他就执行一条。如果想多条sql放在一个事务中执行，则需要使用事务进行处理。当我们开启一个事务，并且没有提交，mysql会自动回滚事务。或者我们使用rollback命令手动回滚事务。
```

约束

```

not null unique  default  primary key   auto_increment

foreign key
	关联表字段数据不能超过被关联表字段的数据范围
	被关联表不能随意的删除和修改关联数据

```

数据类型

```
数值类型 int float 
	整型  
	浮点型  
字符串类型
	char(10)和varchar(10)
	0-255  255字符
日期和时间
date time datetime

枚举和集合
enum set

```

单表语句

```
插入:insert into 表名 values(xx,x,xx),(xx,xx,x);
删除:delete from 表名 where id = 1;
改: update 表名 set 字段名='新值' where id=1;
查:select distinct 字段1,字段2,.... from 库名.表明 
	where 筛选条件
    group by 分组依据字段
    having 过滤条件  分组后过虑  可以使用聚合函数
	order by 排序
	limit 限制显示条数
	
```

多表查询

```
连表 
	inner join ,left join,right join,union
	select employee.name from employee inner join department on employee.dep_id = department.id where department.name='技术';

子查询  某个查询语句作为另外一个语句的查询条件
	select name from employee where dep_id=(select id from department where name='技术');
	

```

索引

```

b+树:提高查询效率

聚集索引
	组织存储整表所有数据的依据
	id primary key
	叶子节点存的是真是数据,存的是整行记录
	
辅助索引(普通索引)
	普通索引建立树形结构,提高查询效率,但是叶子节点存的是该列的数据和对应行的主键值
	index name_index(name)
	unique name_index(name)
	select name from 表名 where name='xx';
	叶子节点找到了对应数据,称为覆盖索引
	找不到列数据,需要回表操作(拿着主键重新走一遍主键的树形结构找到对应行的那一列数据)
索引分类
	主键索引  唯一索引 普通索引
	联合主键索引  联合唯一索引  联合普通索引
	
联合索引的最左匹配特性
多条件查询时
	where name='xx' and age='xx' and sex='xx'
	index name_index(name,age,sex)
	where age='xx' and sex='xx'
	

```



前端内容

HTML  1天

css   2天

js基础+bom和dom操作 2天

jquery  4天

bootstrap  1天

# 今日内容

HTML

```
import socket

server = socket.socket()

server.bind(('192.168.16.160',8001))

server.listen()

while 1:
    conn,addr = server.accept()

    from_browers_msg = conn.recv(1024)
    print('from_browers_msg',from_browers_msg.decode('utf-8'))
    send_msg = b'HTTP/1.1 200 ok\r\n\r\n'
    # data = b'<h1>daguangtou</h1>'
    conn.send(send_msg)
    # conn.send(data)
    with open('test01.html','r',encoding='utf-8') as f:
        data = f.read()
    import time
    data = data.replace('{{ name }}',str(time.time()))
    conn.send(data.encode('utf-8'))

    conn.close()
```

test01.html内容:

```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<h1>24期nb</h1>
{{ name }}
</body>
</html>
```

标签语法

```
　标签的语法：

<标签名 属性1=“属性值1” 属性2=“属性值2”……>内容部分</标签名> 
<标签名 属性1=“属性值1” 属性2=“属性值2”…… />
```

### HTML常用标签

#### 　　head内常用标签

| 标签              | 意义                       |
| ----------------- | -------------------------- |
| <title></title>   | 定义网页标题               |
| <style></style>   | 定义内部样式表             |
| <script></script> | 定义JS代码或引入外部JS文件 |
| <link/>           | 引入外部样式表文件         |
| <meta/>           | 定义网页原信息             |

 

body基本标签

```

不加标签的纯文字也是可以在body中写的
<b>加粗</b>
<i>斜体</i>
<u>下划线</u>
<s>删除</s>

<p>段落标签</p> #独占一个段落

<h1>标题1</h1>
<h2>标题2</h2>
<h3>标题3</h3>
<h4>标题4</h4>
<h5>标题5</h5>
<h6>标题6</h6>

<!--换行-->
<br>

<!--水平线--><hr> #就是单独个一个水平线
每次加上一些内容，别忘了刷新一下页面，才能看到新添加的效果或者内容，其实上面这些标签很少用
```





















