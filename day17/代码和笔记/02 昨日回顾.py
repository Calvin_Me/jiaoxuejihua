# 1.自定义模块
# .py文件就是一个模块
# 模块 -- 工具箱
# 模块里的函数 -- 工具箱的工具

# import 导入
# import time

# from 工具箱 import 工具
# 起别名: import from as

# import from

# from 工具箱 import *
# __all__ = ["要被导入的功能名字"]

# 模块有两种作用:
# 1.脚本  python 文件名
# 2.模块  一个py文件

# def func():
#     print(1234)


# if __name__ == '__main__':
#     for i in range(10):
#         func()


# 模块的导入
#     路径:
#         相对路径: 相对于当前文件
#         绝对路径: sys.path -- 寻找模块的路径

# 模块查找顺序: 内存加载 > 自定义 > 内置 > 第三方
# import datetime
# print(datetime.datetime.now())

# time模块

# time.time()
# time.sleep()
# time.gmtime()
# time.localtime()
# time.strftime("格式的结构",time.gmtime())
# time.mktime(time.strptime("2019-5-20","格式的结构"))

# import datetime
# datetime.datetime.now()
import time
from datetime import datetime,timedelta
# datetime.now()
# print(datetime(2019,5,20))
# print(datetime(2019,5,20) - datetime.now())

# print(datetime.now().timestamp())
# print(datetime.fromtimestamp(time.time()))

# print(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
# print(str(datetime.now()))
# print(datetime.strptime("2019-5-20 13:14:52","%Y-%m-%d %H:%M:%S") - datetime.now())

# print(datetime.now() - timedelta(hours=5))

import random
# 随机数
# random.random() #
# random.uniform(1,10) #
# random.randint(1,10) #
# random.randrange(1,10,2) #
# print(random.choice([1,10,2]))
# print(random.choices([1,10,2,5,7],k=3))
# print(random.sample([1,10,2,5,7],k=3))

# 洗牌 (打乱顺序)
# import random
# lst = [1,2,3,4,65,7,]
# random.shuffle(lst)
# print(lst)