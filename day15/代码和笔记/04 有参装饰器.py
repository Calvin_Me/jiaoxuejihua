# def auth(argv):
#     def wrapper(func):
#         def inner(*args,**kwargs):
#             func(*args,**kwargs)
#         return inner
#     return wrapper


# login_dic = {
#     "username": None,
#     "flag": False
# }
# def auth(argv): # argv == foo
#     def wrapper(func):
#         def inner(*args,**kwargs):
#             if login_dic["flag"]:
#                 func(*args,**kwargs)
#             else:
#                 if argv == "QQ":
#                     user = input("username:")
#                     pwd = input("password:")
#                     if user == "alex" and pwd == "alex123":  # qq
#                         login_dic["flag"] = True
#                         login_dic["username"] = user
#                         func(*args,**kwargs)
#                     else:
#                         print("用户名或密码错误!")
#                 elif argv == "微信":
#                     user = input("username:")
#                     pwd = input("password:")
#                     if user == "1351101501" and pwd == "alex":  # 微信
#                         login_dic["flag"] = True
#                         login_dic["username"] = user
#                         func(*args, **kwargs)
#                     else:
#                         print("用户名或密码错误!")
#                 elif argv == "抖音":
#                     user = input("username:")
#                     pwd = input("password:")
#                     if user == "alexdsb" and pwd == "alex":  # 抖音
#                         login_dic["flag"] = True
#                         login_dic["username"] = user
#                         func(*args, **kwargs)
#                     else:
#                         print("用户名或密码错误!")
#                 else:
#                     user = input("username:")
#                     pwd = input("password:")
#                     if user == "alexdsb@dsb.com" and pwd == "alex":  # 邮箱
#                         login_dic["flag"] = True
#                         login_dic["username"] = user
#                         func(*args, **kwargs)
#                     else:
#                         print("用户名或密码错误!")
#
#         return inner
#     return wrapper
#
# # 错误的案例
# @auth # foo = auth(foo)
# def foo():
#     print("这是一个被装饰的函数")
#
# foo()


login_dic = {
    "username": None,
    "flag": False
}
# 正确的案例
msg = """
QQ
微信
抖音
邮箱
请输入您要选择登陆的app:
"""
chose = input(msg).upper()

def auth(argv):
    def wrapper(func):
        def inner(*args,**kwargs):
            if login_dic["flag"]:
                func(*args,**kwargs)
            else:
                if argv == "QQ":
                    print("欢迎登陆QQ")
                    user = input("username:")
                    pwd = input("password:")
                    if user == "alex" and pwd == "alex123":  # qq
                        login_dic["flag"] = True
                        login_dic["username"] = user
                        func(*args,**kwargs)
                    else:
                        print("用户名或密码错误!")
                elif argv == "微信":
                    print("欢迎登陆微信")
                    user = input("username:")
                    pwd = input("password:")
                    if user == "1351101501" and pwd == "alex":  # 微信
                        login_dic["flag"] = True
                        login_dic["username"] = user
                        func(*args, **kwargs)
                    else:
                        print("用户名或密码错误!")
                elif argv == "抖音":
                    print("来了,老弟!")
                    user = input("username:")
                    pwd = input("password:")
                    if user == "alexdsb" and pwd == "alex":  # 抖音
                        login_dic["flag"] = True
                        login_dic["username"] = user
                        func(*args, **kwargs)
                    else:
                        print("用户名或密码错误!")
                else:
                    print("欢迎登陆dab邮箱")
                    user = input("username:")
                    pwd = input("password:")
                    if user == "alexdsb@dsb.com" and pwd == "alex":  # 邮箱
                        login_dic["flag"] = True
                        login_dic["username"] = user
                        func(*args, **kwargs)
                    else:
                        print("用户名或密码错误!")

        return inner
    return wrapper
@auth("QQ")
def foo():
    print("这是一个被装饰的函数")

# wrapper = auth(chose)
# foo = wrapper(foo)
foo()



"""
# @auth(chose) 相等于以下两行代码的解构
# wrapper = auth(chose)
# foo = wrapper(foo)
"""