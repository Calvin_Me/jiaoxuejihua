# 1.有参装饰器
# 在装饰器的基础上再套一层
# @auth("QQ")
# def foo():
#     pass
# f = auth("qq")
# foo = f(foo)
# foo()


# 2.多个装饰器装饰一个函数
# def wrapper1(func):
#     def inner1(*args,**kwargs):
#         print(1)
#         func(*args,**kwargs)
#         print(11)
#     return inner1
#
# def wrapper2(func):  # func == foo
#     def inner2(*args,**kwargs):
#         func(*args, **kwargs)
#         print(22)
#     return inner2
#
# def wrapper3(func):
#     def inner3(*args,**kwargs):
#         print(3)
#         func(*args, **kwargs)
#         print(33)
#     return inner3

# @wrapper1  # 1 11
# @wrapper3  # 3 33
# @wrapper2  #  8 22

# def foo():
#     print(8)

# 先执行离被装饰的函数最近的语法糖
# 小技巧:进入装饰器从上往下,走到最会一个装饰器执行被装饰的函数,退出装饰器从下往上走