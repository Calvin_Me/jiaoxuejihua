def wrapper1(func):
    def inner1(*args,**kwargs):
        print(1)
        func(*args,**kwargs)
        print(11)
    return inner1

def wrapper2(func):  # func == foo
    def inner2(*args,**kwargs):
        func(*args, **kwargs)
        print(22)
    return inner2

def wrapper3(func):
    def inner3(*args,**kwargs):
        print(3)
        func(*args, **kwargs)
        print(33)
    return inner3

# @wrapper1  # 1 11
# @wrapper3  # 3 33
# @wrapper2  #  8 22

# def foo():
#     print(8)

# foo = wrapper2(foo)  # foo == inner2
# foo = wrapper3(foo)  # inner3 = wrapper3(inner2)
# foo = wrapper1(foo)  # inner1 = wrapper1(inner3)
# foo()                # inner1()


# foo = wrapper3(foo)  # foo == inner3
# foo = wrapper2(foo)  # foo = wrapper2(inner3)  foo == inner2
# foo = wrapper1(foo)  # foo = wrapper1(inner2)


# 被装饰的函数正上方有多个装饰器,先执行离被装饰函数最近的装饰器









