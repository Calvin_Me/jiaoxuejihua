# day1
#     变量:7
#     常量:变量名全部大写就是常量
#     注释: """多行注释""",#单行注释
#     基础数据类型初识:
#       str: python中只要是引号引起来就是字符串
#            + 同类型
#            * 字符串和数字  "a"* 5
#             "''" 配合使用
#       int: 用于计算和比较
#       bool:用于判断
#      输入 input():input获取到得都是字符串  # type()
#      输出 print():打印内容
#      流程控制语句: if  if elif else  if  else  if if  if嵌套

# day2

    # 死循环:while True
    # while else
    # continue : 跳出本次循环,继续下次循环
    # break: 终止当前循环

    # 字符串格式化:
    #     %s:填充字符串(填充数字)
    #     %d/%i:填充数字
    #     %%:转义
    # f"{变量}"  3.6版本及以上才能使用

    # 运算符
    #     算数运算符  + - * / // ** %
    #     比较运算符  > < >= <= == !=
    #     赋值运算符  = += -= *= /= //= **= %=
    #     逻辑运算符
    #         and and都为真 选and后面的 都为假 选and前面的 一真一假 选假
    #         or  or都为真 选or前面的 都为假 选or后面的 一真一假 选真
    #         not 相反
    #         () > not > and > or
    #     成员运算符
    #         in not

    # 编码初识:
    #     ascii 不支持中文
    #     gbk 英文 1 中文 2
    #     unicode 英文 2 中文 4
    #     utf-8 英文 1 欧洲 2 亚洲3

    # 1B(字节) = 8b(位)

# day3
#     整型:
#         10 - 2 :算法
#         2 - 10: 算法
#     字符串详解
#         索引
#         切片
#         步长
#         全部大写
#         全部小写
#         以什么开头
#         以什么结尾
#         统计
#         脱
#         分割
#         替换

        # is系列:
        # 判断是不是十进制
        # 判断是不是阿拉伯数字
        # 判断是不是字母和数字和中文
        # 判断是不是字母和中文

    # len() 共用方法

    # for 循环
    # for i in "alex":
        # print(i)

    # 非可迭代对象:int,bool

# day4
    # list: 列表
    # 列表存储大量数据,不同类型

    # 增:append insert extend
    # 删:pop remove clear del
    # 改:lst[0] = 12 lst[0:4] = "asdfasdfasdfasdfasd"  lst[0:4:2] = 12,14
    # 查: for lst[0]

    # tuple() 元组
    # 元组就是一个不可变的列表
    # index count
    # for

    # range()
    # range(1,10)
    # range(1,10,3)

    # py2:range(1,10) -- list
    # py3:range(1,10) -- range(1,10)

# day5
#     dict:字典:
#       {"键":"值"}
#        "键":不可变数据,保证唯一
#         "值": 任意
#     增: dic["键"] = "值"  setdefault("键","值") setdefault("键")
#     删: pop clear popitem del
#     改: update dic["键"] = "值"
#     查:setdefault("键") get("键") dic["键"] keys values items

    # 解构
        # a = 10
        # b = 20
        # a,b = b,a
        # a,b = (10,12)
        # a,b = [10,12]

# day6
#     小数据池:
        # 整型,字符串,布尔值
        # 整型: -5 ~ 256  python3.7  -5 ~ 4096
        # 字符串: 乘法的时候总长度不能超过20

      # 代码块: 模块(文件),函数,终端中每一个行
        # 整型,字符串,布尔值
        # 整型: -5 ~ 正无穷
        # 字符串: 乘法的时候总长度不能超过20

      # is == 区别:
        #is 判断的内存地址时候相同
        # == 判断两边的值是否相同

     # 深浅拷贝:
     #    赋值: 多个变量名指向同一个内存地址,修改一个就会全部改变
     #    浅拷贝: 只拷贝第一层,修改第一层不进行改变
     #    深拷贝: 不可变数据类型共用一块空间,可变数据类型开辟新的空间  -- 不管有多少层

     # 集合:
     # set() 天然去重
     # 集合就是一个没有值的字典
     # 增 add update(迭代添加)
     # 删 pop remove clear del
     # 改 先删后加
     # 查 for

# day7
#     数据类型补充:
#         str:
#             首字母大写
#             每个单词首字母大写
#             大小写反转
#             查找 find index
#             拼接 join
#             居中 -- 填充
#             字符串格式化.format
#         list:
#             lst1 + lst2
#             lst1 * 5 -- 元素是共用的
#             排序
#             反转
#         tuple:
#             lst1 + lst2
# #           lst1 * 5 -- 元素是共用的

#         dict:
#             dict(a=1,b=2)
#             dic = {}
#             dic.fromkeys("abc",[])
#             print(dic)

    # 坑:
    #      lst = [1,2,3,4,5]
    #      从后先前删除
         # 拷贝一份,循环新的数据,删除旧的数据

          # 字典和集合
          # 迭代的时候不能改变源数据的大小

    # 二次编码:
    #     编码: encode()
    #     解码: decode()

# day8
#     文件操作:
#     r w a
#     r+ w+ a+
#     rb wb ab

     #f = open("文件路径")
     # f = read
     # f.close()

     # 路径:
     #    绝对路径
     #    相对路径  -- 推荐使用

     # read()  # r 字符 rb 字节
     # seek()  # 字节移动

     # with open() as f:
     #     with 同时操作多个文件

# day9
#     函数初识:
#         函数定义
    #         def 函数名():
    #             函数体
    #     函数调用:
    #         def 函数名():
    #             函数体
    #         函数名+()

        # 函数的返回值:
        #       def 函数名():
        #           函数体
        #           return "str",[1,2,3]
        #       print(函数名())

        # return :不写和写了return不写值 返回None
        # return :终止函数
        # return :下方代码不会执行
        # return :返回多个元素的时候,是元组的形式返回
        # return :可以写多个,但是只执行第一个

        # 函数名参数:
        #     def 函数名(位置参数,默认参数):
        #         print(位置参数,默认参数)
        #     函数名(位置参数一一对应,可写可不写,写的就会覆盖原来的默认参数的值)

        # 形参: 在函数定义阶段括号中的是形参
        # 实参: 在函数调用阶段括号中的是实参
        # 传参: 将实参传递给形参的过程就是传参

        # 优先级: 位置 > 默认

# day10
#     动态参数:
         # *args,**kwargs  -- 万能传参
         # 动态参数,动态关键字参数
         # 优先级: 位置 > 动态位置 > 默认 > 动态关键字

      # 函数的注释:
           #写函数要写注释
           # 查看注释 函数名.__doc__
           # 查看函数名 函数名.__name__

      # 名称空间:
      #       内置空间: python解释器自带的
      #       全局空间: 当前py文件
      #       局部空间: 函数体中

      # 作用域:
      #       全局作用域: 内置空间 + 全局空间
      #       局部作用域: 局部空间

      # 嵌套函数:

      # global: 只修改全局变量
      # nonlocal:只修改局部,修改离它最近的一层

      # 三元运算符: 条件成立的结果 条件 条件不成立的结果

# day11
#      函数名的第一类对象及使用
#         1.可以当做值被赋值
#         2.可以当做元素存放到容器中
#         3.可以当做另一个函数的参数
#         4.可以当做另一个函数的返回值

     # 可迭代对象: 具有__iter__()方法就是可迭代对象
     # 迭代器:具有__iter__()和__next__()方法就是迭代器
     # 迭代器是python自带的

     # 迭代器:
        # for 的本质
        # 优点:节省内存,惰性机制
        # 缺点: 不能逆行,不能灵活使用,不能直接查看元素 一次性的

    # 递归:
    #     1.不断调用自己本身
    #     2.有明确的终止条件

# day12
#     生成器:
        # 生成器的本质就是迭代器
        # 生成器和迭代器的区别:
        #     生成器是程序员写的
        #     迭代器是python自带

        # def func():
        #     yield "话"
        #
        # func()   # 生成一个生成器
        # print(func().__next__())
        # print(func().__next__())
        # print(func().__next__())
        # print(func().__next__())

        # def func():
        #     yield "话"
        #
        # g = func()   # 生成一个生成器
        # print(next(g))
        # print(next(g))
        # print(next(g))
        # print(next(g))

        # yield from [11,22,33,44]
        # 把一个可迭代对象中的元素逐个返回

        # for循环来进行取值,list(生成器)

        # 推导式:
        #     list [变量(加工后变量) for循环 加工条件]
        #     生成器表达式 (变量(加工后变量) for循环 加工条件)
        #     dict {"键":"值" for循环 加工条件}
        #     set {变量(加工后变量) for循环 加工条件}

        # 内置函数一:
        #     """
        #     all() any() bytes()
        #     callable()
        #     chr() complex()
        #     divmod() eval()
        #     exec() format()
        #     frozenset() globals()
        #     hash() help() id() input()
        #     int() iter() locals()
        #     next() oct() ord()
        #     pow() repr() round()
        #     """

# day13

    # 内置函数二:
        #"""
        #abs() enumerate()  max() min() open() range() print() len() list() dict() str()
        #float() reversed() set() sorted() sum() tuple() type() zip() dir()
        #"""
        # filter() map() reversed() sorted() zip() print()  max() min() reduce()
        # lambda x:x
        # lambda :x
        # lambda x:(1,2,4,6)

    # 闭包:
    #     1.在一个嵌套内,使用非全局变量(且不是本层变量)就是闭包

# day14
#     装饰器:

        # 开放封闭原则:在不修改源代码和调用方式的前提下,额外增加一些新的功能!

        # 标准版(装饰器):
        # def func(a):  #a是要被装饰的函数名
        #     def foo(*args,**kwargs):
        #         "装饰之前的操作"
        #         ret = a(*args,**kwargs)
        #         "装饰之后的操作"
        #         return ret
        #     return foo
        # @func
        # def f1(*args,**kwargs):
        #     print(f"这是一个{args}")
        #     return "我可以返回了"

# day15
    # 见day15 07今日总结