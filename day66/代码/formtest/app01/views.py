from django.shortcuts import render,redirect,HttpResponse

# Create your views here.

from django import forms

# class UserInfo(forms.Form):
#
#     username=forms.CharField(
#         label='用户名：',
#         # widget=forms.widgets.TextInput,
#
#     )
#     password=forms.CharField(
#         label='密码：',
#
#         widget=forms.widgets.PasswordInput(attrs={'class':'c1'}),
#     )

    # sex = forms.ChoiceField(
    #     choices=((1,'女'),(2,'男'),),
    #     # widget=forms.RadioSelect,
    #     # widget=forms.widgets.Select,
    # )
    #
    # hobby = forms.MultipleChoiceField(
    #     choices= ((1,'喝酒'),(2,'抽烟'),(3,'烫头')),
    #     # widget=forms.SelectMultiple,
    #     widget=forms.CheckboxSelectMultiple,
    # )
    #
    # remember_me = forms.ChoiceField(
    #     label='记住我',
    #
    #     widget=forms.CheckboxInput,
    # )
    #
    # bday = forms.DateField(
    #     label='出生日期',
    #     widget=forms.DateInput(attrs={'type':'date'}),  # 日期类型必须加上日期属性
    #
    #
    # )


from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError


import re
# 自定义验证规则
def mobile_validate(value):
    mobile_re = re.compile(r'^(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}$')
    if not mobile_re.match(value): #a范德萨水电费
        raise ValidationError('手机号码格式错误')  #自定义验证规则的时候，如果不符合你的规则，需要自己发起错误


class UserInfo(forms.Form):

    username=forms.CharField(
        label='用户名：',
        # initial='张三'  #默认值
        # min_length=6, #最小值为6位
        # required=True,  #不允许为空,值为False就可以为空
        # widget=forms.widgets.TextInput,
        # max_length=8,

        error_messages={
            'required':'不能为空',
            'min_length':'太短了！',
            'max_length':'太长了！',
        },
        # validators=[RegexValidator(r'^a','必须以a开头！'),RegexValidator(r'b$','必须以b结尾！')]
        # validators=[mobile_validate,]  #a范德萨水电费

    )

    password=forms.CharField(
        label='密码：',
        widget=forms.widgets.PasswordInput(attrs={'class':'c1'},render_value=True),

    )

    r_password = forms.CharField(
        label='确认密码：',
        widget=forms.widgets.PasswordInput(attrs={'class': 'c1'}, render_value=True),

    )

    # 局部钩子
    def clean_username(self):
        value = self.cleaned_data.get('username')
        if '666' in value:
            raise ValidationError('光喊666是不行的！')
        else:
            return value

    # 全局钩子
    def clean(self):
        password = self.cleaned_data.get('password')
        r_password = self.cleaned_data.get('r_password')

        if password == r_password:
            return self.cleaned_data
        else:
            # raise ValidationError('两次密码不一致！！！！')
            self.add_error('r_password','两次密码不一致~~~~') # 给单独的某个字段加错误信息



def index(request):

    if request.method == 'GET':
        u_obj = UserInfo()


        return render(request,'index.html',{'u_obj':u_obj})

    else:

        u_obj = UserInfo(request.POST)  #{'username':'','password':'123'}
        print(u_obj.fields)
        # u_obj.is_valid()  #校验用户提交的数据，全部校验成功返回True，任意一个失败都返回False
        if u_obj.is_valid():
            # {'username': 'a范德萨水电费', 'password': '1111'}
            print('正确数据',u_obj.cleaned_data)  #校验成功之后的数据cleaned_data

            return HttpResponse('ok')
        else:
            # print('错误信息',u_obj.errors)

            return render(request,'index.html',{'u_obj':u_obj})



