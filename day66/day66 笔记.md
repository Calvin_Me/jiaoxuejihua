# 昨日内容回顾

## 事务和锁

```
from django.db import transaction

@transaction.atomic
def index(request):
	..

def index(request):
	...
	
	#上下文
	with transaction.atomic():
		...
		
    ...
    
锁:
	models.Book.objects.select_for_update().filter(id=1)

```



## 中间件

```

五个方法
process_request
process_response
process_view
process_exception
process_template_response


自定义中间件
应用下 创建一个文件夹,例如xx文件夹
创建一个py文件,oo.py

from django.utils.deprecation import MiddleWareMixin

class MyAuth(MiddleWareMixin):
	
	def process_request(self,request):
		request
		...
	def process_response(self,request,response):
		return response
	
```



请求--wsgi.py--中间件(process_request)--urls.py--视图(数据库:orm,template:html)--中间(process_response)





# 今日内容

## form组件

```
1 生成html标签
2 保留原来的数据
3 校验用户提交的数据
```



正则校验器

```
from django.core.validators import RegexValidator
 
class MyForm(Form):
    user = fields.CharField(
        validators=[RegexValidator(r'^[0-9]+$', '请输入数字'), RegexValidator(r'^159[0-9]+$', '数字必须以159开头')],
    )
```



校验函数

```
 
# 自定义验证规则
def mobile_validate(value):
    mobile_re = re.compile(r'^(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}$')
    if not mobile_re.match(value):
        raise ValidationError('手机号码格式错误')  #自定义验证规则的时候，如果不符合你的规则，需要自己发起错误

class MyForm(Form):
    user = fields.CharField(
        validators=[mobile_validate，],
    )

        
```



局部钩子

```

class LoginForm(forms.Form):
    username = forms.CharField(
        min_length=8,
        label="用户名",
        initial="张三",
        error_messages={
            "required": "不能为空",
            "invalid": "格式错误",
            "min_length": "用户名最短8位"
        },
        widget=forms.widgets.TextInput(attrs={"class": "form-control"})
    )
    ...
    # 定义局部钩子，用来校验username字段,之前的校验股则还在，给你提供了一个添加一些校验功能的钩子
    def clean_username(self):
        value = self.cleaned_data.get("username")
        if "666" in value:
            raise ValidationError("光喊666是不行的")
        else:
            return value
```

全局钩子

```
class LoginForm(forms.Form):
    ...
    password = forms.CharField(
        min_length=6,
        label="密码",
        widget=forms.widgets.PasswordInput(attrs={'class': 'form-control'}, render_value=True)
    )
    re_password = forms.CharField(
        min_length=6,
        label="确认密码",
        widget=forms.widgets.PasswordInput(attrs={'class': 'form-control'}, render_value=True)
    )
    ...
    # 定义全局的钩子，用来校验密码和确认密码字段是否相同，执行全局钩子的时候，cleaned_data里面肯定是有了通过前面验证的所有数据
    def clean(self):
        password_value = self.cleaned_data.get('password')
        re_password_value = self.cleaned_data.get('re_password')
        if password_value == re_password_value:
            return self.cleaned_data #全局钩子要返回所有的数据
        else:
            self.add_error('re_password', '两次密码不一致') #在re_password这个字段的错误列表中加上一个错误，并且clean_data里面会自动清除这个re_password的值，所以打印clean_data的时候会看不到它
            #raise ValidationError('两次密码不一致')
```



大致流程

```
字段内部属性相关校验--局部钩子校验---然后循环下一个字段进行上面两步校验 -- 最后执行全局钩子
```









































