# 1.装饰器:
# 装饰器 -- 作用在不修改源码及调用方式前提下额外增加一些功能
# 开放封闭原则

# 函数嵌套:
# def func():
#     def foo():
#         print(1)
#     return foo
# func()()

# 闭包:
# def func(a):
#     def foo():
#         print(a)
#     return foo
# func(1)()

# low版装饰器
# def func(a):
#     def foo():
#         print(a)
#     return foo
#
#
# def f1():
#     print("这是一个f1")
#
# f1 = func(f1)
# f1()

# 被装饰的函数能够接受参数
# def func(a):
#     def foo(*args,**kwargs):
#         a(*args,**kwargs)
#
#     return foo
#
# def f1(*args,**kwargs):
#     print(f"这是一个{args}")
#
# f1 = func(f1)
# f1(1,2,3,34,4,5)

# 高级
# def func(a):
#     def foo(*args,**kwargs):
#         a(*args,**kwargs)
#
#     return foo
#
# @func
# def f1(*args,**kwargs):
#     print(f"这是一个{args}")
#
# f1(1,2,3,34,4,5)

# 被装饰的函数返回值
# def func(a):
#     def foo(*args,**kwargs):
#         ret = a(*args,**kwargs)
#         return ret
#     return foo
#
# @func
# def f1(*args,**kwargs):
#     print(f"这是一个{args}")
#     return "我可以返回了"
# f1(1,2,3,34,4,5)


# 标准版(装饰器):
# def func(a):  #a是要被装饰的函数名
#     def foo(*args,**kwargs):
#         "装饰之前的操作"
#         ret = a(*args,**kwargs)
#         "装饰之后的操作"
#         return ret
#     return foo
# @func
# def f1(*args,**kwargs):
#     print(f"这是一个{args}")
#     return "我可以返回了"
# f1(1,2,3,34,4,5)
