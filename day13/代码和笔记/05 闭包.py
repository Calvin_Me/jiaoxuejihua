# def func():
#     a = 1
#     def f1():
#         def foo():
#             print(a)
#         return foo
#     return f1
# ret = func()
# a = ret()
# a()

# func()()()

# 1.在嵌套函数内,使用非全局变量(且不是本层变量) -- 就是闭包

# avg_lst = []
# def func(pirce):
#     avg_lst.append(pirce)
#     avg = sum(avg_lst) / len(avg_lst)
#     return avg
# print(func(150000))
# print(func(160000))
# print(func(170000))
# print(func(150000))
# avg_lst.append(18888888)


# def func(pirce):
#     avg_lst = []
#     avg_lst.append(pirce)
#     avg = sum(avg_lst) / len(avg_lst)
#     return avg
# print(func(150000))
# print(func(160000))
# print(func(170000))
# print(func(150000))


# def func():
#     avg_lst = []  # 自由变量
#     def foo(pirce):
#         avg_lst.append(pirce)
#         avg = sum(avg_lst) / len(avg_lst)
#         return avg
#     return foo
# ret = func()()

# print(ret(150000))
# print(ret(160000))
# print(ret(170000))
# print(ret(150000))
# print(ret(180000))
# print(ret.__closure__)
# (<cell at 0x0000018E93148588: list object at 0x0000018E931D9B08>,)

# __closure__ 判断是否是闭包

# 了解:
# print(ret.__code__.co_freevars)  # 获取的是自由变量
# print(ret.__code__.co_varnames)  # 获取的是局部变量

# 闭包的作用:
# 1.  保证数据的安全性
# 2.  装饰器