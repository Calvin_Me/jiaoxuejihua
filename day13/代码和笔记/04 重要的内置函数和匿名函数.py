# def func():
#     print(123)
#     return "你好"
#
# func()

# def func(a,b):
#     return a+b
# print(func(1,2))

# 匿名函数:
# f = lambda x,y:(x,y)
# print(f(1,2))
# print(f.__name__)

# def func():
#     return 1
#
# print(func())

# print((lambda x:x)(2)) # 同一行定义 同一行调用

# lambda 关键字 -- 定义函数
# x,y  形参
#:x+y  返回值 -- 只能返回一个数据类型

# lst = [lambda i:i*i for i in range(10)]
# print(lst[2](2))

# lst = []
# for i in range(10):
#     def func(i):
#         return i*i
#     lst.append(func)
# print(lst[2](3))

# lst = [lambda :i*i for i in range(10)]
# print(lst[2]())

# for i in range(10):
#     pass
# print(i)

# lst = []
# for i in range(10):
#     def func():
#         return i*i
#     lst.append(func)
# print(lst[2]())

# 一行函数
# 形参可以不写
# 返回值必须要写,返回值只能返回一个数据类型


# lst = list((lambda i:i*i for i in range(5)))
# print(lst[1](4))

# lst = [x for x in (lambda :i**i for i in range(5))]
# print(lst[2]())

# lst1 = []
# def func():
#     for i in range(5):
#         def foo():
#             return i**i
#         yield foo
#
# for x in func():
#     lst1.append(x)
# print(lst1[2]())


# 内置函数(重要)
# print(format(13,">20"))  # 右对齐
# print(format(13,"<20"))  # 左对齐
# print(format(13,"^20"))  # 居中

# print(format(13,"08b"))    # 2
# print(format(13,"08d"))    # 10
# print(format(13,"08o"))    # 8
# print(format(12,"08x"))    # 16

# print(bin(13))

# filter()  # 过滤

# lst = [1,2,3,4,5,6,7]
# def func(s):
#     return s > 3
# print(list(filter(func,lst)))
# func就是自己定义一个过滤条件,lst要迭代的对象
# lst = [1,2,3,4,5,6,7]
# print(list(filter(lambda x:x % 2 == 1,lst)))

# map() # 对象映射
# print(list(map(lambda x:x*x,[1,2,3,8,4,5])))
# 对可迭代对象中每个元素进行加工


# 反转
    # lst = [1,2,3,4,5]
    # lst.reverse()
    # print(lst)

    # lst1 = list(reversed(lst))
    # print(lst)
    # print(lst1)


# lst = [1,23,34,4,5,213,123,41,12,32,1]
# print(sorted(lst))   # 升序
# print(lst)

# lst = [1,23,34,4,5,213,123,41,12,32,1]
# print(sorted(lst,reverse=True))  # 降序

# dic = {"key":1,"key1":2,"key3":56}
# print(sorted(dic,key=lambda x:dic[x],reverse=True))  # key是指定排序规则

# print(max([1,2,-33,4,5],key=abs))  # key指定查找最大值的规则

# from functools import reduce
# # reduce 累计算
# print(reduce(lambda x,y:x-y,[1,2,3,4,5]))