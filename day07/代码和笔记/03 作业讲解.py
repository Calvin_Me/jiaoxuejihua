"""
3.写代码实现

循环提示用户输入，如果输入值在v1中存在，则追加到v2中，如果v1中不存在，则添加到v1中。（如果输入N或n则停止循环）
"""
# v1 = {'alex','武sir','黑哥'}
# v2 = []

# print(set("alex"))
# while True:
#     a = input("请输入:")  # "ALEX"
#     if a.upper() != "N":
#         if set(a) < v1:  # set("alex") {'x', 'e', 'a', 'l'}
#             v2.append(a)
#         else:
#             v1.add(a)
#     else:
#         break
#     print(v1,v2)

# while True:
#     ret = input(">>>>")
#     if ret in v1:
#         v2.append(ret)
#     elif ret.upper() == "N":
#         break
#     else:
#         v1.add(ret)
#     print(v1, v2)


# v1 = {'k1':'v1','k2':[1,2,3]}
# v2 = {'k1':'v1','k2':[1,2,3]}

# result1 = v1 == v2
# result2 = v1 is v2
# print(result1)
# print(result2)

# v1 = {'k1':'v1','k2':[1,2,3]}
# v2 = v1
#
# result1 = v1 == v2  # True
# result2 = v1 is v2  # True
# print(result1)
# print(result2)

# v1 = {'k1':'v1','k2':[1,2,3]}
# v2 = v1
#
# v1['k1'] = 'wupeiqi'
# print(v2)

# v1 = '人生苦短，我用Python'
# v2 = [1,2,3,4,v1]  # [1,2,3,4,'人生苦短，我用Python']
# v1 = "人生苦短，用毛线Python"
# print(v2)

# info = [1,2,3]
# userinfo = {'account':info, 'num':info, 'money':info}
#
# info.append(9)
# print(userinfo)
#
# info = "题怎么这么多"
# print(userinfo)

# info = [1,2,3]
# userinfo = [info,info,info,info,info]
#
# info[0] = '不仅多，还特么难呢'
# print(info,userinfo)

# info = [1,2,3]
# userinfo = [info,info,info,info,info]  # [[1,2,3],[1,2,3],["闭嘴",2,3],[1,2,3][1,2,3]]
#
# userinfo[2][0] = '闭嘴'
# print(info,userinfo)

# info = [1, 2, 3]  # [1,"是谁说Python好学的？",3]
# user_list = [] # [[1,2,3],...[1,2,3]]
# for item in range(10):
#     user_list.append(info)
#
# info[1] = "是谁说Python好学的？"
#
# print(user_list)

# data = {}
# for i in range(10):
#     pass
# data['user'] = i
# print(data)  #{"user":9}

# data_list = []
# data = {}
# for i in range(10):
#     data['user'] = i
#     data_list.append(data)
# print(data_list)

# data_list = []
# for i in range(10):
#     data = {}
#     data['user'] = i
#     data_list.append(data)
# print(data_list)

"""
20.敲七游戏. 从1开始数数. 遇到7或者7的倍数（不包含17,27,这种数）要在桌上敲⼀下. 
编程来完成敲七. 给出⼀个任意的数字n. 从1开始数. 数到n结束. 把每个数字都放在列表中,
在数的过程中出现7或 者7的倍数（不包含17,27,这种数）.则向列表中添加⼀个'咣'
例如, 输⼊10. lst = [1, 2, 3, 4, 5, 6, '咣', 8, 9, 10]
"""
# lst = []
# num = input("请输入数字:")
# for i in range(1,int(num)+1):
#     if i % 7 == 0:
#         lst.append("咣")
#     else:
#         lst.append(i)
# print(lst)