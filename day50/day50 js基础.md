# 前端内容回顾

## HTML

```
标签分类
	块级标签:div p h1-h6 form hr br ul li ol table标签
	内联标签:span a img label input select textarea 

```



input标签

```
type:
text,password,date,radio(name),checkbox,submit,button,reset,hidden,file
```

特殊字符

```
&nbsp; 空格
&gt; &lt;
```

select标签

```
<select name='' ,multiple>
	<option value='1' 
	selected>xx</option>

</select>
selected默认选中

```

table标签

```
<table>
	<thead>
		<tr>
			<th>标题</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>xxx</td>
		</tr>
	</tbody>
	
</table>
```



ul和ol标签

```
<ul type='none'>
	<li>xx1</li>
	<li>xx2</li>
</ul>

<ol start='2'>
	<li>xx1</li>
	<li>xx2</li>
</ol>
```

label标签

```
<label> 
	用户名:  input
	select
	...
</label>

<label for='id值'>用户名</label>
<input id='id值'>
```



## css

### css三种引入方式

```
<div id='id值' style='div'></div>

head标签
	style标签
		div{
			color:red;
		}
		
head标签
<link rel='stylesheet' href='css文件路径'>
```



### 选择器

#### 基础选择器

```
标签名{}
#id值{}
.类值{}  
```

通用选择器

```
*{}
```

组合选择器

```
div p
div>p
div+p
div~p

div,p{color:red;}
```

属性选择器

```
[属性名]{}
[属性名='属性值']{}
div[属性名]{}
div[属性名='属性值']{}
```

伪类选择器

```
a:link{}
a:visited{}
a:active{}
a:hover{}
input:focus{}

```



伪元素选择器

```
div:first-letter{}
div:before{}

.clearfix:after{
	content:'';
	display:block;
	clear:both;
}

```

优先级

```
继承 0
元素选择器 1
类  10
id  100
内敛  1000 

display:block!important;  最牛逼

权重值相加不进位
```



css样式相关

宽高

```
width:100px;
height:100px;
```

字体相关

```
字体:font-family:'宋体',
字体大小:font-size:10px; 默认大小16px
字重:font-weight:bold;
字体颜色:color:red;#ff0000,rgb(255,0,0),rgba(255,0,0,0.3)  0.3透明度
```

文本

```
text-align:center;..
```

背景

```
background:#ff0000 url('图片地址') no-repeat 100px 200px;
```

border 边框

```

border:1px solid red;
border-left:1px solid red; ...

border-radius:50%;  设置圆角
```

盒子模型

```
content:内容 
padding:100px 100px 20px 20px;
border:
margin
```



display:

```
display:inline;
block
inline-block

none 隐藏标签  不占位
visibility:hidden;   占位
```



float浮动

```
float:left;
float:right;

浮动起来的标签:可以设置高度宽度,不独占一行
父级标签塌陷问题:
	脱离正常文档流
1 父级设置高度
2  给父级标签加上clearfix这个类值
	.clearfix:after{
        content:'';
        display:block;
        clear:both;
    }

```



# 注意点:

```
<div class='c1 c2 c3 c4'></div>  

c2和c1相同的样式,会覆盖c1的,使用c2的
class类值可以是多个,中间空格分割
```



定位:

```
四种:static  
postion:relative  top,left,right,bottom
postion:absolute; 
postion:fixed;  按照窗口位置进行移动
```

z-index:

```
z-index:100;
```

opacity:标签透明度



## js

引入方式:

```
<script src='js文件路径'></script>
<script>
	alert('xx');
</script>
```



变量

```
var AngleBaby = '你媳妇';
```

### 数据类型

#### number类型

```

var a = 1;
var a = 1.11;
var a = 1e10;
```

####  查看类型:

```
typeof a;
```



#### string字符串

```
var a = 'hello world!';
```

字符串常用方法

```
var a = 'hello xxx';
var b = 'hello ooo';

字符串拼接
	var c = a + b;
a.length; -- 字符串长度
a.tirm(); -- 去除空格
a.split('分隔符',1)
a.concat(b)  字符串拼接
a.indexOf(元素)  查看元素的索引位置
a.charAt(n) 通过索引找元素
a.slice(n,m) 切片
```

字符串转换为数值

```
parseInt('1') -- 1
parseFloat('1.11') -- 1.11
```

布尔值

```
var a = true;
var b = false;
数据类型都有布尔值: '',0,null,undefined,NaN...都是false
```



#### null和undefined

```
null表示值是空，一般在需要指定或清空一个变量时才会使用，如 name=null;

undefined表示当声明一个变量但未初始化时，该变量的默认值是undefined。还有就是函数无明确的返回值时，返回的也是undefined。
	var a; -- undefined
	
null表示变量的值是空，undefined则表示只声明了变量，但还没有赋值。
```



#### object类型

```
var a = 'xx';
var b = new String('oo');
```

##### 数组

```
var a = [11,22,33];
typeof a; -- "object"

var b = new Array([11,22,33,44]);
typeof b; -- "object"
```

###### 数组常用方法

```
var a = [11,22,33];
索引取值 -- a[0];
数组长度 -- a.length;
尾部追加 -- a.push(44);
尾部删除 -- a.pop()
	示例:
		var a = [11, 22, 33, 44];
		var b = a.pop();
		结果:
            a -- [11, 22, 33]
            b -- 44
头部添加 -- a.unshift('aa')
	示例:
		 var a = [11, 22, 33];
		 a.unshift('aa')
		 a --  ["aa", 11, 22, 33]
头部删除 -- shift()
	示例:
		var a = ["aa", 11, 22, 33];
		a.shift() -- 'aa'
		a -- [11, 22, 33];

切片 -- slice()
	var b = a.slice(0,3);
	b -- [11, 22]
反转 reverse()
	var a = [11,22,33];
	a.reverse() 
	a -- [33,22,11]

数组元素拼接 join
	示例:
		var a = ['aa','bb','cc'];
		var b = a.join('_');
		b -- "aa_bb_cc";

数组合并 concat
	var a = ["aa", "bb", "cc"];
	var b = [11,22];
	var c = a.concat(b);
	c -- ["aa", "bb", "cc", 11, 22];
排序 sort 比较尬
	示例:
		var a = [12,3,25,43];
		对a进行升序排列:
		1 定义函数
			function sortNumber(a,b){
                return a - b
             };
		2 var b = a.sort(sortNumber)
		b -- [3, 12, 25, 43]
	sort 规则:
		  如果想按照其他标准进行排序，就需要提供比较函数，也就是自己提供一个函数提供排序规则，该函数要比较两个值，然后返回一个用于说明这两个值的相对顺序的数字。比较函数应该具有两个参数 a 和 b，其返回值如下：
　　　　　　若 a 小于 b，在排序后的数组中 a 应该出现在 b 之前，则返回一个小于 0 的值。
　　　　　　若 a 等于 b，则返回 0。
　　　　　　若 a 大于 b，则返回一个大于 0 的值。

删除 .splice() 
	示例:
		var a = ['aa','bb',33,44];
		单纯删除:a.splice(1,1)
		a -- ["aa", 33, 44]
		
		删除在替换新元素:
		var a = ["aa", 33, 44];
		a.splice(0,2,'hello','world');
		a --  ["hello", "world", 44];
	三个参数介绍:
		参数：1.从哪删(索引), 2.删几个  3.删除位置替换的新元素(可多个元素)
```



##### 自定义对象 -- python字典

```
索引取值
	var a = {'name':'alex','age':48};
	键可以不加引号:var a = {name:'alex',age:48};
	a['age']; -- 48
	a.age; -- 48
```



##### 类型查询

### ![img](https://images2015.cnblogs.com/blog/315302/201702/315302-20170205172450401-644571910.png)



### 运算符

##### 算数运算符

```
+ - * / % ++ --  i++,是i自加1，i--是i自减1   i++的这个加1操作优先级低，先执行逻辑，然后再自加1，而++i，这个加1操作优先级高，先自加1，然后再执行代码后面的逻辑

示例:
	var a = 100;
	a++;或者++a; -- 101 a自增1

	a++和++a的区别,示例:
	var a = 102;
	a++ == 102; -- true
	a -- 103;
	++a == 103; -- false
	a -- 104;

```

##### 比较运算符

```
> >= < <= != == === !==

==(弱等于)和===(强等于)两者的区别:
	示例:
		var a = 11;
		var b = '11';
		a == b -- true
         a === b; -- false

```

##### **逻辑运算符**

```
&& || !  #and，or，非（取反）!null返回true
示例:
	var a = true;
    var b = true;
    var c = false;
    a && b; -- true
    a && c; -- false
    a || c; -- true
    !c; -- true
```

##### 赋值运算符

```
= += -= *= /= 
示例: n += 1其实就是n = n + 1
```

### 流程控制

#### if判断

```
简单if-else判断
	var a = 4;
	if (a > 5){
        console.log('a大于5');

    }
    else{
        console.log('小于5');
    };

多条件判断
var a = 10;
if (a > 5){
  console.log("a > 5");
}else if(a < 5) {
  console.log("a < 5");
}else {
  console.log("a = 5");
}
```

#### switch 切换

```


示例:
	var a = 1;
	switch (a++){ //这里day这个参数必须是一个值或者是一个能够得到一个值的算式才行，这个值和后面写的case后面的值逐个比较，满足其中一个就执行case对应的下面的语句，然后break，如果没有加break，还会继续往下判断
        case 1:
            console.log('等于1');
            break;
        case 3:
            console.log('等于3');
            break;
        default:  case都不成立,执行default
            console.log('啥也不是!')	

    }
	

```

#### for循环

```
for (var i=0;i<10;i++) {  //就这么个写法，声明一个变量，变量小于10，变量每次循环自增1，for(;;){console.log(i)}；这种写法就是个死循环，会一直循环，直到你的浏览器崩了，就不工作了，回头可以拿别人的电脑试试~~
  console.log(i);
}
循环数组：
var l2 = ['aa','bb','dd','cc']
方式1
for (var i in l2){
   console.log(i,l2[i]);
}
方式2
for (var i=0;i<l2.length;i++){
　　console.log(i,l2[i])
}

循环自定义对象：
var d = {aa:'xxx',bb:'ss',name:'小明'};
for (var i in d){
    console.log(i,d[i],d.i)  #注意循环自定义对象的时候，打印键对应的值，只能是对象[键]来取值，不能使用对象.键来取值。
}


```

while循环

```
var i = 0;
var a = 10;
while (i < a){
	console.log(i);
	if (i>5){
		continue;
		break;
	}
	i++;
};
```

#### 三元运算

```
var c = a>b ? a:b;  
```

### 函数

#### 定义函数

```
普通函数
function f1(){
	console.log('111');
}
f1();  执行函数

带参数的函数
function f1(a,b){
	console.log('111');
}
f1(1,2);

带返回值的函数
function f1(a,b){
	return a+b;
}
f1(1,2); -- 3

返回值不能有多个
function f1(a,b){
	return a,b;
}
f1(1,2); -- 2
function f1(a,b){
	return [a,b];  想要多个返回值,需要换一种数据类型
}
f1(1,2); -- [1, 2]

匿名函数:
	var f1 = function(){
        console.log('111');
    }
    f1();

自执行函数
    (function(a,b){
        console.log(a+b);
    })(1,2);
```



#### 函数的全局变量和局部变量

```
局部变量：

	在JavaScript函数内部声明的变量（使用 var）是局部变量，所以只能在函数内部访问它（该变量的作用域是函数内部）。只要函数运行完毕，本地变量就会被删除。

全局变量：

	在函数外声明的变量是*全局*变量，网页上的所有脚本和函数都能访问它。

变量生存周期：

    JavaScript变量的生命期从它们被声明的时间开始。

    局部变量会在函数运行以后被删除。

    全局变量会在页面关闭后被删除。
```



#### 作用域

```
首先在函数内部查找变量，找不到则到外层函数查找，逐步找到最外层。

var city = "BeiJing";
function f() {
  var city = "ShangHai";
  function inner(){
    var city = "ShenZhen";
    console.log(city);
  }
  inner();
}
f();  


var city = "BeiJing";
function Bar() {
  console.log(city);
}
function f() {
  var city = "ShangHai";
  return Bar;
}
var ret = f();
ret();

闭包:
	var city = "BeiJing";
    function f(){
        var city = "ShangHai";
        function inner(){
            console.log(city);
        }
        return inner;
    }
    var ret = f();
    ret();

```

#### 面向对象

```
function Person(name){
	this.name = name;
};

var p = new Person('taibai');  

console.log(p.name);

Person.prototype.sum = function(a,b){  //封装方法
	return a+b;
};

p.sum(1,2);
3
```



### date对象

```
//方法1：不指定参数
var d1 = new Date(); //获取当前时间
console.log(d1.toLocaleString());  //当前2时间日期的字符串表示
//方法2：参数为日期字符串
var d2 = new Date("2004/3/20 11:12");
console.log(d2.toLocaleString())

常用方法
var d = new Date(); 
使用 d.getDate()
//getDate()                 获取日
//getDay ()                 获取星期 ，数字表示（0-6），周日数字是0
//getMonth ()               获取月（0-11,0表示1月,依次类推）
//getFullYear ()            获取完整年份
//getHours ()               获取小时
//getMinutes ()             获取分钟
//getSeconds ()             获取秒
//getMilliseconds ()        获取毫秒
//getTime ()                返回累计毫秒数(从1970/1/1午夜),时间戳

```





























































































