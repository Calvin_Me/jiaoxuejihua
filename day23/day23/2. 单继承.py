# class Human:
#
#     def __init__(self, name, sex, age):
#         self.name = name
#         self.age = age
#         self.sex = sex
#
#
# class Dog:
#
#     def __init__(self, name, sex, age):
#         self.name = name
#         self.age = age
#         self.sex = sex
#
#
# class Cat:
#
#     def __init__(self, name, sex, age):
#         self.name = name
#         self.age = age
#         self.sex = sex


# 单继承

# class Animal:
#
#     def __init__(self, name, sex, age):
#
#         self.name = name
#         self.age = age
#         self.sex = sex
#
#
# class Human(Animal):
#     pass


# class Dog(Animal):
#     pass
#
#
# class Cat(Animal):
#     pass


# class Cat(object):
#     pass

# cat1 = Cat()
# Human,Dog,Cat 子类,派生类.
# Animal 父类,基类,超类.

# person = Human('李业', '男', 18)
# print(person.name)

# class Animal:
#
#     live = '有生命的'
#
#     def __init__(self, name, sex, age):
#
#         self.name = name
#         self.age = age
#         self.sex = sex
#
#     def eat(self):
#         print('动物都需要进食')
#
#
# class Human(Animal):
#
#     live = '有思想的活着'

# 1. 类名执行父类属性方法
# print(Human.live)
# Human.eat(111)

# class Animal:
#
#     live = '有生命的'
#
#     def __init__(self, name, sex, age):
#
#         self.name = name
#         self.age = age
#         self.sex = sex
#
#     def eat(self):
#         print(self)
#         print('动物都需要进食')
#
#
# class Human(Animal):
#
#     body = '有头有脸'


# 2. 派生类对象 执行父类的属性方法
# obj = Human('汪洋', '男', 48)
# a1 = Animal('汪洋', '男', 48)
# print(a1.body)
# print(obj.live)
# obj.eat()
# print(obj)

# 查询顺序单向不可逆: 子类使用父类的属性方法,父类不能使用子类的属性方法.


# 3. 既要执行子类的方法,又要执行父类的方法

# 方法一: 不依赖继承的
# class Animal:
#
#     def __init__(self, name, sex, age):
#
#         self.name = name
#         self.age = age
#         self.sex = sex
#
#
# class Human:
#
#    def __init__(self,name, sex, age, hobby):
#        '''
#
#        :param name: 李业
#        :param sex: 男
#        :param age: 18
#        :param hobby: 旅游
#        '''
#        # self = obj
#        # Animal.__init__(人类对象,姓名,性别,年龄)
#        Animal.__init__(self,name,sex,age)
#        self.hobby = hobby
#
#
# class Dog(Animal):
#     pass
#
#
# class Cat(Animal):
#     pass

# obj = Human('驴友')
# obj2 = Human('抽烟')
# print(obj.__dict__)
# obj = Human('李业','男',18,'旅游')
# print(obj.__dict__)
# def func(self):
#     self = 666
#     print(self)
#
# self = 3
# # func(self)
# func(666)


# def func1(a,b):
#     print(a,b)
#
#
# def func2(argv1,argv2,x):
#     func1(argv1,argv2)
#     print(x)
#
# func2(1,2,666)


# 方法二: 依赖于继承

class Animal:

    def __init__(self, name, sex, age):

        self.name = name
        self.age = age
        self.sex = sex

    def eat(self):
        print('动物都需要吃饭')

class Human(Animal):

    def __init__(self, name, sex, age, hobby):

       # Animal.__init__(self,name,sex,age)
       # super(Human,self).__init__(name, sex, age)  #完整的写法
        super().__init__(name,sex,age)  # 执行父类的__init__方法,重构父类方法.
        self.hobby = hobby

    def eat(self):
        print(f'{self.name}都需要吃饭')


# class Dog(Animal):
#     pass
#
#
# class Cat(Animal):
#     pass
#
obj = Human('李业','男',18,'旅游')
obj.eat()
# print(obj.__dict__)


# 单继承的课堂练习

# 1
# class Base:
#     def __init__(self, num):
#         self.num = num
#     def func1(self):
#         print(self.num)
#
# class Foo(Base):
#     pass
# obj = Foo(123)
# obj.func1()



# 2
# class Base:
#     def __init__(self, num):
#         self.num = num
#
#     def func1(self):
#         print(self.num)
#
# class Foo(Base):
#
#     def func1(self):
#         print("Foo. func1", self.num)
#
# obj = Foo(123)
# print(obj.__dict__)
# obj.func1()

# 3
# class Base:
#     def __init__(self, num):
#         self.num = num
#     def func1(self):
#         print(self.num)
# class Foo(Base):
#     def func1(self):
#         print("Foo. func1", self.num)
# obj = Foo(123)
# obj.func1()
# # 4
# class Base:
#
#     def __init__(self, num):
#         self.num = num
#
#     def func1(self):  # self= obj
#         print(self.num)  # 123
#         self.func2()  # obj.func2()
#
#     def func2(self):
#         print("Base.func2")
#
# class Foo(Base):
#     def func2(self):
#         print("Foo.func2")
#
# obj = Foo(123)
# obj.func1()
# # 再来
# class Base:
#     def __init__(self, num):
#         self.num = num
#
#     def func1(self):
#         print(self.num)
#         self.func2()
#
#     def func2(self):
#         print(111, self.num)
#
# class Foo(Base):
#     def func2(self):
#         print(222, self.num)
#
#
# lst = [Base(1), Base(2), Foo(3)]
#
# for obj in lst:
#     obj.func2()




'''
111 1
111 2
222 3

'''
#
# # 再来

# class Base:
#     def __init__(self, num):
#         self.num = num
#
#     def func1(self):  # self = Foo(3)
#         print(self.num)
#         self.func2()
#
#     def func2(self):
#         print(111, self.num)
#
# class Foo(Base):
#     def func2(self):
#         print(222, self.num)
#
# lst = [Base(1), Base(2), Foo(3)]
#
# for obj in lst:
#     obj.func1()

'''
1
111 1
2
111 2
3
222 3
'''






