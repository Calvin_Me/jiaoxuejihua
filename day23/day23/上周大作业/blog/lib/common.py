from core import src
import logging.config
from conf import settings
import os
def auth(func):
    def inner(*args, **kwargs):
        if src.status_dic['status']:
            ret = func(*args, **kwargs)
            return ret
        else:
            print('\033[1;31;0m 请先进行登录 \033[0m')
            if src.login():
                ret = func(*args, **kwargs)
                return ret

    return inner


# def check_file_exists(file_path):
#     if os.path.exists(file_path):
#         return True


def get_logger(log_path):

    # path = r'F:\s24\day21\liye.log'
    log_dic = settings.LOGGING_DIC
    log_dic['handlers']['file']['filename'] = log_path
    logging.config.dictConfig(log_dic)  # 导入上面定义的logging配置
    logger = logging.getLogger()  # 生成一个log实例
    return logger
