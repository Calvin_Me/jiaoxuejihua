# class A(object):
#     pass



# class A:
#     pass
#
#
# class B(A):
#     pass
#
# class C(B):
#     pass


# class A:
#     pass
#
#
# class B(object):
#     pass


# class ShenXian:
#
#     def fly(self):
#         print('神仙都会飞')
#
#     def walk(self):
#         print('神仙会走路')
#
# class Monkey:
#
#     def climb(self):
#         print('猴子都会爬树')
#
#     def walk(self):
#         print('猴子会走路')
#
# class SunWuKong(ShenXian,Monkey):
#     pass
#
# sun = SunWuKong()
# # sun.fly()
# # sun.climb()
# sun.walk()

# 经典类: 深度优先.从左至右,深度优先.

class O:
    name = '太白'

class D(O):
    pass

class E(O):
    name = '李业'
    # pass
class F(O):
    name = '宝元'

class B(D,E):
    pass

class C(E,F):
    pass

class A(B,C):
    pass

obj = A()
print(obj.name)

# mro(Child(Base1，Base2)) = [ Child ] + merge( mro(Base1), mro(Base2), [ Base1, Base2] )
# mro(A(B,C)) = [A] + merge(mro(B),mro(C),[B,C])

'''
mro(A(B,C)) = [A] + merge(mro(B),mro(C),[B,C])

mro(B(D,E)) = [B] + merge(mro(D),mro(E),[D,E])

mro(B(D,E)) = [B] + merge([D,O],[E,O],[D,E])
mro(B(D,E)) = [B,D] + merge([O],[E,O],[E])
mro(B(D,E)) = [B,D,E] + merge([O],[O])
mro(B(D,E)) = [B,D,E,O]

mro(C) = [C,E,F,O]

mro(A(B,C)) = [A] + merge([B,D,E,O],[C,E,F,O],[B,C])
            = [A,B] + merge([D,E,O],[C,E,F,O],[C])
            = [A,B,D] + merge([E,O],[C,E,F,O],[C])
            = [A,B,D,C] + merge([E,O],[E,F,O])
            = [A,B,D,C,E] + merge([O],[F,O])
            = [A,B,D,C,E,F,O] 
           

'''
# print(A.mro())
