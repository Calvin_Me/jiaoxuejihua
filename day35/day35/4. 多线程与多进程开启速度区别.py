# 多进程:

from threading import Thread
# from multiprocessing import Process
# import os
#
# def work():
#     print('hello')
#
# if __name__ == '__main__':
#     #在主进程下开启线程
#     t=Process(target=work)
#     t.start()
#     print('主线程/主进程')




# 多线程
# from threading import Thread
# import time
#
# def task(name):
#     print(f'{name} is running')
#     time.sleep(1)
#     print(f'{name} is gone')
#
#
#
# if __name__ == '__main__':
#
#     t1 = Thread(target=task,args=('海狗',))
#     t1.start()
#     print('===主线程')  # 线程是没有主次之分的.
