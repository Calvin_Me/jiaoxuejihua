
# from threading import Thread
# from threading import currentThread
# from threading import enumerate
# from threading import activeCount
# import os
# import time
#
# x = 3
# def task():
#     # print(currentThread())
#     time.sleep(1)
#     print('666')
# print(123)
# if __name__ == '__main__':
#
#     t1 = Thread(target=task,name='线程1')
#     t2 = Thread(target=task,name='线程2')
#     # name 设置线程名
#     t1.start()
#     t2.start()
#     # time.sleep(2)
#     # print(t1.isAlive())  # 判断线程是否活着
#     # print(t1.getName())  # 获取线程名
#     # t1.setName('子线程-1')
#     # print(t1.name)  # 获取线程名  ***
#
#     # threading方法
#     # print(currentThread())  # 获取当前线程的对象
#     # print(enumerate())  # 返回一个列表,包含所有的线程对象
#     print(activeCount())  # ***
#     print(f'===主线程{os.getpid()}')
