import classpay


if 2 > 1:
    # 实例化对象,调用支付接口
    content = input('请输入支付方式:').strip()
    money = input('请输入钱数:').strip()
    # content: QQpay Alipay Wechat
    if content == 'QQpay':
        obj = classpay.QQpay()
        classpay.pay(obj,int(money))

    elif content == 'Alipay':
        obj = classpay.Alipay()
        classpay.pay(obj, int(money))

    elif content == 'Wechat':
        obj = classpay.Wechat()
        classpay.pay(obj, int(money))