# while -- 关键字 (死循环)
#
# if 条件:
#     结果
#
# while 条件：
#     循环体

# print(1)
# while True:
#     print("痒")
#     print("鸡你太美")
#     print("卡路里")
#     print("好运来")
#     print("小三")
#     print("小白脸")
#     print("趁早")
#     print("过火")
# print(2)

# falg = True
# while falg:
#     print(1)
# print(2)



# print(bool(0))
# 数字中非零的都是True


# 正序的示范
# count = 1
# while count <= 5:
#     print(count)
#     count = count + 1


# 倒序的示范
# count = 5
# while count:
#     print(count)
#     count = count - 1


# 正序打印从 25 - 57
# count = 25
# while count <= 57:
#     print(count)
#     count = count + 1


# 倒序打印从 57 - 25
# count = 57
# while count >= 25:
#     print(count)
#     count = count - 1

# break
# continue

# while True:
#     print(123)
#     print(234)
#     break   # 终止当前循环，break下方的代码不会进行执行
#     print(345)
# print(1111)

# while True:
#     print(123)
#     print(234)
#     print(345)

# while True:
#     print(123)
#     print(234)
#     continue
#     #continue 伪装成循环体中的最后一行代码(跳出当前循环继续下次循环)
#     #continue 下面的代码不会执行
#     print(345)
# print(1111)

# while else

# while True:
#     print(111)
#     break
# else:
#     print(222)

# while True:
#     print(111)
#     break
# print(222)



# 总结：
    # 打断循环的方式：
    #     1.自己修改条件
    #     2.break
    # break -- 打破当前循环 （终止当前循环）
    # continue -- 跳出当前循环继续下次循环（将continue伪装成循环体中的最后一个行代码）
    # break和continue相同之处：他们以下的代码都不执行