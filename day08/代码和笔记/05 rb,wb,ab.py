# rb
# f1 = open("1.jpg","rb")
# print(f1.read())   # 全部读取
# print(f1.read(3))  # 按照字节读取

# wb
# f = open("3.jpg","wb")
# f.write(f1.read())

# ab
# f = open("2.jpg","ab")
# f.write("你好啊".encode("utf-8"))

# f = open("2.jpg","rb")
# print(f.read())


# +操作
# r+: 读写 - 先读后写

# 错误的操作 (坑)
# f = open("萝莉小姐姐电话号","r+",encoding="utf-8")
# f.write("常鑫你太美")
# print(f.read())


# 正确的操作:
# f = open("萝莉小姐姐电话号","r+",encoding="utf-8")
# print(f.read())
# f.write("常鑫你太美")

# w+ 清空写,读

# f = open("萝莉小姐姐电话号","w+",encoding="utf-8")
# f.write("常鑫你太美")
# print(f.tell())
# # f.seek(15)         #
# print(f.tell())
# print(f.read())


# a+ 追加写,读
# f = open("萝莉小姐姐电话号","a+",encoding="utf-8")
# f.write("常鑫你太美")
# print(f.tell())   # 字节数
# print(f.seek(0,0))  # 0将光标移动到文件的头部
# print(f.read())



# 其他操作:
# tell 查看光标    --- 返回的是字节数
# seek 移动光标
#     1.seek(0,0)  -- 移动到文件的头部
#     2.seek(0,1)  -- 当前位置
#     3.seek(0,2)  -- 移动到文件的末尾
#     4.seek(3)    -- 按照字节进行移动(按照编码集,自己进行计算)

# f = open("萝莉小姐姐电话号","r",encoding="gbk")
# print(f.read(3)) # 字符

# f = open("萝莉小姐姐电话号","rb")
# print(f.read(3))   # 字节

# f = open("萝莉小姐姐电话号","r",encoding="gbk")
# print(f.read(3))     # 字符

# 错误操作
# f = open("萝莉小姐姐电话号","r",encoding="gbk")
# f.seek(-1)
# print(f.read())

# 文件修改

# f = open("萝莉小姐姐电话号","r",encoding="gbk")
# s = f.read()
# s1 = s.replace("你太美","吃煎饼")
#
# f1 = open("萝莉小姐姐电话号","w",encoding="gbk")
# f1.write(s1)

# with 关键字 open("萝莉小姐姐电话号","r",encoding="gbk") as f:
#   文件操作的具体内容


# w,a 检测有文件就操作,没文件就创建

# with open("萝莉小姐姐电话号","r",encoding="gbk")as f,\
#         open("萝莉小姐姐电话号.txt","w",encoding="gbk")as f1:
#     for i in f:
#         s1 = i.replace("大烧饼","井盖")
#         f1.write(s1)
#
# import os
# os.rename("萝莉小姐姐电话号","萝莉小姐姐电话号.bak")
# os.rename("萝莉小姐姐电话号.txt","萝莉小姐姐电话号")

# with open("萝莉小姐姐电话号","r",encoding="gbk")as f:
#     pass # 缩进里操作文件
# print(f.read())  # 文件及已经关闭了
