# r操作:
# f = open("萝莉小姐姐电话号",mode="r",encoding="utf-8")
# print(f.read())    #全部读取
# print(f.read(5))   #按照字符进行读取
# print(f.read())

# print(f.readline())  # 读取一行内容,自动换行
# print(f.readline())  # "常鑫你就是大帅比\n"
# print(f.readline().strip()) #去除换行符

# print(f.readlines())  # 一行一行的读取,存放在列表中

# 解决大文件:
# for i in f:
#     print(i)  # 本质就是一行一行进行读取

# print(f.readline())
# print(f.readline())
# print(f.readline())

# w操作:
# w分为两步:
#     1.先清空文件
#     2.写入文件

# f = open("萝莉小姐姐电话号","w",encoding="utf-8")
# f.write("123456789\n")
# f.write("123456789\n")
# f.write("123456789\n")
# f.write("123456789\n")


# 路径:
# 1.绝对路径 -- C:\user\meet\python24\萝莉小姐姐电话号
# 2.相对路径

# 绝对路径方式打开文件
# f = open("F:\s24\day08\萝莉小姐姐电话号","r",encoding="utf-8")
# print(f.read())

# 相对路径方式打开文件
# f = open("../day03/萝莉小姐姐电话号","r",encoding="utf-8")
# print(f.read())
# ../ 返回上一层

# f = open(r"C:\user\net\s24\day03\萝莉小姐姐电话号","r",encoding="utf-8")

# 路径转义:
#     1."\\"
#     2.r"C:\user\net"
# 推荐使用相对路径 (*****)

# s = "[1,'2',3,4]"
# print(s)
# print(repr(s))  # repr -- 显示数据原生态

# a操作:  追加写
# 在源文件的基础上进行添加
# f = open("../day03/萝莉小姐姐电话号","a",encoding="utf-8")
# f.write("138383848\n")
# f.write("138383850\n")

