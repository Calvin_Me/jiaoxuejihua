

# 昨日内容回顾



## HTML标签

### 

```
块级标签:独占一行,可以包含内联标签和部分块级标签
内联标签:不独占一行,只能包含内联标签,不能包含块级标签
```

### 常用标签

```
div标签和span标签
a标签:超链接标签
	href属性没有的话,就是普通的文本标签
	href属性有但是没有值,点击刷新当前页面,并且文本有了效果,有下划线,还有文本颜色
	href属性而且有值,那么点击这个a标签,会跳转到href属性的值指定的网址上
		打开页面方式两种:
			target='_blank' 在新的标签页打开
			target='_self'  默认值,在当前标签页窗口打开页面

img标签:图片标签
	<img src='图片路径' alt='图片加载不成功的时候的提示信息' title='鼠标悬浮显示内容' width='100' height='100'>

```

### table标签:表格标签

```

<table>
	<thead>
		<tr>  #一行
			<th>姓名</th>  
			<th>爱好</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>常鑫</td>
			<td>自己的大腿</td>
		</tr>
	</tbody>
</table>
```



### input标签

```
type属性:
	text:普通文本输入框
	password:密文
	date:日期选择框
	radio:单选框
	checkbox:复选框
	submit:提交按钮
	button:普通按钮
	reset:重置
	file:文件上传
	hidden:隐藏input框
```

### select下拉框

```
单选下拉框:
<select name='city'>
	<option value='a'>沙河</option>
	<option value='b'>沙河</option>
</select>  
提交的数据:
	city:a

多选下拉框
<select name='hobby' multiple='multiple'>
	<option value='a'>肤白</option>
	<option value='b'>貌美</option>
	<option value='c'>大长腿</option>
</select>
提交的数据:
	hobby:['a','b','c']

```

### textarea多行文本

```
<textarea row='3' col='10' maxlength='10'></textarea>
```

### form表单标签

```
action='网址'  将数据提交到某个网址去
input  
	输入框name属性要写 :name='username'  username:用户输入的内容
	选择框,每个选项的name和value属性都要写  name='sex' value='1' -- sex:1
```

### 标签属性

```
disabled='disabled' 简写 disabled
readonly='readonly'
checked='checked'  设置input选择框 默认选中效果 <input type='radio' checked>
selected='selected'  设置select标签下面的option标签默认选中,<option selected>
```

### label标签

```
方式1:
	<label for='username'>用户名:</label>
	<input id='username'>
方式2:
	<label>
		用户名:<input id='username'>
	</label>
	
```



# 今日内容

## css介绍

```
CSS（Cascading Style Sheet，层叠样式表)定义如何显示HTML元素，给HTML设置样式，让它更加美观。
```



## 语法结构

```
	div{
            color:green;
            background-color: black;
        }
        

选择器{css样式:样式对应的值}
        
```

css引入方式

```
方式1: 内部样式
	head标签中写一下内容:
	    <style>
        	div{
            	color:green;
           		background-color: black;
        	}
    	</style>
方式2:  行内样式(内联样式)
	<div style="color:yellow;background-color: black;">
    	床前明月光,地上鞋三双
	</div>

方式3:(常用)  外部样式
	第一步:创建一个css文件
	第二步:在html文件中引入:<link rel="stylesheet" href="test.css(路径)">  <!-- 引入css文件 -->
	第三步:css文件中样式的写法
	div{color:green;xx:xx;...}

```

## 选择器

### 基本选择器

```
元素选择器:(标签名)
	p {color: "red";}
	
id选择器:按照id属性来找到对应的标签
	#id属性对应的值{css属性:属性值}
	示例:
		<div id="d2" class="c1">
            床上狗男女,其中就有你1
        </div>
		#d1{color:red;}
类选择器:
	.class属性对应的值{css属性:属性值}
	示例:
		<div id="d2" class="c1">
            床上狗男女,其中就有你1
        </div>
		.c1{color:red;}
	
```



### 组合选择器

#### 后代选择器

```
选择器 空格 选择器
.c1 a{
	color:green;
}
示例:
	<p>贵人肾宝</p>
    <div class="c1">
        <a href="">葫芦娃</a>
        <div>
            <a href="">太白</a>
            <div>
                <a href="">李业</a>
            </div>
        </div>
    </div>
    <p>你好</p>
    <p>他也好</p>
    <div class="c2">
        <a href="">葫芦娃2</a>
    </div>
    <p>大家好</p>
```

#### 儿子选择器

```
.c1>a{
	color:green;
}
示例:同上
```

#### 毗邻选择器

```
.c1+p{
	color:green;
}
找的是紧挨着class属性为c1的标签的下面的标签
示例:同上
```

#### 弟弟选择器

```
.c1~p{
	color:green;
}
示例:同上
```



### 属性选择器

```
通过标签属性来找到对应的标签
通过属性来找写法:
	[xxx]{color:red;} 找到有xxx属性的所有标签
	[xxx='p2']{color:red;} 找到有xxx属性的并且属性值为p2的所有标签
	p[title]{xx:xx;}  找到所有有title属性的p标签
 	p[title='p2']{xx:xx;} 找到所有有title属性的并且属性值为p2的p标签
 	示例:
		   <!DOCTYPE html>
            <html lang="en">
            <head>
                <meta charset="UTF-8">
                <title>Title</title>
                <style>
                    p[xxx='p2']{
                        color:red;
                    }
                </style>

            </head>
            <body>
                <p>p1</p>
                <p xxx="p2">p2</p>
                <p xxx="p3">p3</p>
            </body>
            </html>
```



### 组合选择器

```
写法:
	div,p{
            color:red;
        }
解释:div选择器和p选择器找到的所有标签设置共同的样式.
示例:
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>

    <style>
        /*div{*/
        /*    color:red;*/
        /*}*/
        /*p{*/
        /*    color:red;*/
        /*}*/
        div,p{
            color:red;
        }
    </style>
</head>
<body>
    <div>div1</div>
    <p>p1</p>
</body>
</html>
```

### 伪类选择器

```

示例代码:
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        a:link{  /* a标签访问前设置样式 */
            color:red;
        }
        a:active{  /* a标签鼠标点下去显示样式 */
            color: green;
        }
        a:visited{ /* a标签访问后显示样式 */
            color: pink;
        }
        a:hover{ /* 鼠标悬浮到a标签时显示样式 */
            color:purple;
        }
        div:hover{   /* 鼠标悬浮到div标签时显示样式 */
            background-color: green;
        }
        input:focus{ /* input标签捕获光标时的样式显示 */
            background-color: orange;
        }
    </style>
</head>

<body>

    <a href="http://www.92py.com/">校草网</a>

    <div>

    </div>

    <input type="text">

</body>
</html>
```



### 伪元素选择器

```
first-letter:文本内容首字母设置
	示例:
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        div:first-letter{
            color:red;
            font-size: 40px;
        }
    </style>
</head>
<body>
    <div>
        鹅鹅鹅,曲项向天歌
    </div>

</body>
</html>

```

before 示例:

```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        div:before{
            content: '?';
            color:red;
        }
    </style>
</head>
<body>

<div>
    鹅鹅鹅,曲项向天歌
</div>
</body>
</html>
```

after示例

```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        div:after{
            content: '?';
            color:red;
        }
    </style>
</head>
<body>
    <div>
        鹅鹅鹅,曲项向天歌
    </div>
</body>
</html>
```



## css权重

![img](https://images2018.cnblogs.com/blog/867021/201803/867021-20180305155201408-1680872107.png)

```
权重越高,对应选择器的样式会被优先显示
组合选择器,各选择器的权重相加
权重不进位,11类选择器组合到一起,也没有一个id选择器的优先级大,小就是小
默认css样式是可以继承的,继承的权重为0
权重相同的选择器,谁后写的,用谁的
```

示例代码:

```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>

        div .c1{
            color:red;
        }

        .c1 .c2 .c3{
            color:green;
        }

        #d1{
            color:yellow;
        }

        div{
            color:green;
        }

        .c2{
            color:red;
        }

    </style>
</head>
<body>

<div class="c2">
    霜叶红于二月花
    <div class="c1" id="d1" style="color:blue;">
<!--    <div class="c1">-->
        停车坐爱枫林晚
    </div>
</div>

</body>
</html>
```



a标签设置样式,需要选中设置,不能继承父级标签的样式

示例:

```
.c3 a{
	color:red;
}

<div class="c3">
    <a href="">百度</a>
</div>
```

















