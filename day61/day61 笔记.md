# 昨日内容回顾

批量插入

```

obj_list = []

for i in range(10):
	obj = models.Book(
		title='xx'
	)
	obj_list.append(obj)
models.Book.objects.bulk_create(obj_list)

```

查询

```
all()  queryset
filter(id=1,name='xx')  and  queryset
get()  model对象 
count()
first()
last()
exclude() exclude(id=1)
exists()  False  True
order_by('id','-price')
reverse() 反转,先排序
values()  -- queryset([{},{}])
values_list() queryset([(),()])
distinct()  
```

filter 双下划线查询

```
filter(price__gt=30)
filter(price__gte=30)
filter(price__lt=30)
filter(price__lte=30)
filter(price__range=[30,40])
filter(price__in=[30,40,50..])
filter(title_contains='py')
filter(title_icontains='py')
filter(title_istartswith='py')
filter(title_iendswith='py')
filter(pub_date__year__gt='2018',pub_date__month='09',pub_date__day='09')
```



多表建立表关系

```
OneToOneField('表名',to_field='字段名',on_delete=CASCADE)
ForeignKey('表名',to_field='字段名',on_delete=CASCADE)
ManyToManyField('表名')
```



## 增删改查

### 增加

```
# 增加
    # 一对一
    # au_obj = models.AuthorDetail.objects.get(id=4)


    models.Author.objects.create(
        name='海狗',
        age=59,
        # 两种方式
        au_id=4
        # au=au_obj
    )

    # 一对多
    # pub_obj = models.Publish.objects.get(id=3)
    #
    # models.Book.objects.create(
    #     title='xx2',
    #     price=13,
    #
    #     publishDate='2011-11-12',
    #     # publishs=pub_obj , #类属性作为关键字时,值为model对象
    #     publishs_id=3  # 如果关键字为数据库字段名称,那么值为关联数据的值
    # )

    # 多对多  -- 多对多关系表记录的增加
    # ziwen = models.Author.objects.get(id=3)
    # haigou = models.Author.objects.get(id=5)

    new_obj = models.Book.objects.create(
        title='海狗产后护理第二部',
        price=0.5,
        publishDate='2019-09-29',
        publishs_id=2,
    )

    new_obj.authors.add(3,5)  #  #*args  **kwargs
    new_obj.authors.add(*[3,5])  # 用的最多,
    new_obj.authors.add(ziwen, haigou)
```

删除

```
# 删除
    # 一对一
    # models.AuthorDetail.objects.filter(id=3).delete()
    # models.Author.objects.filter(id=3).delete()
    # 一对多
    # models.Publish.objects.filter(id=3).delete()
    # models.Book.objects.filter(id=4).delete()

    # 多对多
    book_obj = models.Book.objects.get(id=2)
    # book_obj.authors.add()  # 添加
    # book_obj.authors.remove(1)  #删除
    # book_obj.authors.clear()  # 清除
    # book_obj.authors.set(['1','5'])  # 先清除再添加,相当于修改
```

改

```
    # 改
    # ret = models.Publish.objects.get(id=2)
    # models.Book.objects.filter(id=5).update(
    #     # title='华丽丽',
    #     publishs=ret,
    #     # publishs_id=1,
    # )
```



###  基于对象的跨表查询

```

    # 查询
    # 一对一
    # 关系属性写在表1,关联到表2,那么通过表1的数据去找表2的数据,叫做正向查询,返过来就是反向查询
    # 查询一下王洋的电话号码

    # 正向查询  对象.属性
    # obj = models.Author.objects.filter(name='王洋').first()
    # ph = obj.au.telephone
    # print(ph)

    # 查一下电话号码为120的作者姓名
    # 反向查询  对象.小写的表名
    # obj = models.AuthorDetail.objects.filter(telephone=120).first()
    # ret = obj.author.name  #陈硕
    # print(ret)

    # 一对多
    # 查询一下 海狗的怂逼人生这本书是哪个出版社出版的  正向查询
    # obj = models.Book.objects.filter(title='海狗的怂逼人生').first()
    # ret = obj.publishs.name
    # print(ret)  #24期出版社
    #  查询一下 24期出版社出版过哪些书
    # obj = models.Publish.objects.filter(name='24期出版社').first()
    #
    # ret = obj.book_set.all() #<QuerySet [<Book: 母猪的产后护理>, <Book: 海狗的怂逼人生>]>
    # for i in ret:
    #     print(i.title)

    # 多对多
    # 海狗的怂逼人生 是哪些作者写的 -- 正向查询
    # obj = models.Book.objects.filter(title='海狗的怂逼人生').first()
    # ret = obj.authors.all()
    #
    # print(ret)  #<QuerySet [<Author: 王洋>, <Author: 海狗>]>
    # for i in ret:
    #     print(i.name)

    # 查询一下海狗写了哪些书 -- 反向查询
    # obj = models.Author.objects.filter(name='海狗').first()
    # ret = obj.book_set.all()
    # print(ret)
    # for i in ret:
    #     print(i.publishs.name)
    #     print(i.title)
    # return HttpResponse('ok')
```



admin添加用户

```
python manage.py createsuperuser
输入用户名:wuchao
邮箱不用输 直接回车
输入密码:必须超过8位,并且别太简单

```



admin注册

```
from django.contrib import admin

# Register your models here.

from app01 import models

admin.site.register(models.Author)
admin.site.register(models.AuthorDetail)
admin.site.register(models.Publish)
admin.site.register(models.Book)
```































